<?php

// | PHP Version 4                                                        |
// | This source file is subject to version 3.0 of the PHP license,       |

class String_Random
{
    var $_patterns = array();
    var $_old_patterns = array();
    var $_max = 10;

    function String_Random($max = null)
    {
        $this->_old_patterns = $this->_getOldPatterns();
        $this->_patterns     = $this->_getPatterns();
        if ($max) {
            $this->_max = $max;
        }
    }

        
    /**
     * Returns a random string that will match the regular expression passed in the list argument
     *
     * The following regular expression elements are supported
     *      \w    Alphanumeric + "_".
     *      \d    Digits.
     *      \W    Printable characters other than those in \w.
     *      \D    Printable characters other than those in \d.
     *      .     Printable characters.
     *      []    Character classes.
     *      {}    Repetition.
     *      *     Same as {0,}.
     *      ?     Same as {0,1}.
     *      +     Same as {1,}.
     * 
     * @param array|string $patterns
     * @return array|string
     * @access public
     */
    function getRandRegex($patterns)
    {
        mt_srand();
        if (is_array($patterns)) {
            $string_array = array();
            foreach ($patterns as $v) {
                $string = String_Random::_randregex($v);
                array_push($string_array, $string);
            }
            return $string_array;
        } else {
            return String_Random::_randregex($patterns);
        }
    }

        
    /**
     * Returns a random string based on the concatenation of all the pattern strings in the list
     *
     * The following pre-defined patterns are supported
     *      c    Any lowercase character [a-z]
     *      C    Any uppercase character [A-Z]
     *      n    Any digit [0-9]
     *      !    A punctuation character [~`!@$%^&*()-_+={}[]|\:;"'.<>?/#,]
     *      .    Any of the above
     *      s    A "salt" character [A-Za-z0-9./]
     *      b    Any binary data
     * 
     * @param array|string $patterns
     * @return array|string
     * @access public
     */
    function getRandPattern($patterns)
    {
        mt_srand();
        if (is_array($patterns)) {
            $string_array = array();
            foreach ($patterns as $v) {
                $string = String_Random::_randpattern($v);
                array_push($string_array, $string);
            }
            return $string_array;
        } else {
            return String_Random::_randpattern($patterns);
        }
    }

        
    /**
     * Adds new random string pattern
     *
     * For example, if you wanted a pattern A
     * that contained all upper and lower case letters ([A-Za-z]),
     *
     *    $newPattern = array_merge(range('A', 'Z'), range('a', 'z'));
     *    $sr->addPattern('A', $newPattern);
     *
     * @param string $ch
     * @param array $patterns
     * @return boolean clear 
     * @access public
     */
    function addPattern($ch, $patterns)
    {
        if (is_array($patterns)) {
            $this->_old_patterns[$ch] = $patterns;
            return array_key_exists($ch, $this->_old_patterns);
        } else {
            return false;
        }
    }

        
    /**
     * @param string  $patterns
     * @return string
     * @access private
     */
    function _randregex($patterns)
    {
        $string_array = array();
        $string = '';

        $chars = preg_split('//', $patterns, -1, PREG_SPLIT_NO_EMPTY);

        while ($ch = array_shift($chars)) {
            $regchCheck = String_Random::_regch_check($ch);
            if ($regchCheck) {
                list($chars, $string_array) =
                    String_Random::_regch($ch, $chars, $string_array);
            } elseif (preg_match("/[\$\^\*\(\)\+\{\}\]\|\?]/", $ch)) {
                print "'$ch' not implemented.  treating literally.\n";
                array_push($string_array, $ch);
            } else {
                array_push($string_array, $ch);
            }
        }

        foreach ($string_array as $ch) {
            $string .= $ch[mt_rand(0, count($ch)-1)];
        }
        return $string;
    }

        
    /**
     * @param string  $ch
     * @return boolean
     * @access private
     */
    function _regch_check($ch)
    {
        if ($ch === "\\" || $ch === '.' || $ch === '['
            || $ch === '*' || $ch === '+' || $ch === '?' || $ch === '{'
            ) {
            $result = true;
        } else {
            $result = false;
        }

        return $result;
    }

        
    /**
     * @param string  $ch
     * @param array $chars
     * @param array $string
     * @return array
     * @access private
     */
    function _regch($ch, $chars, $string)
    {
        if (is_a(@$this, 'String_Random')) {
            $maxCount = $this->_max;
            $patterns = $this->_patterns;
        } else {
            $patterns = String_Random::_getPatterns();
            $maxCount = 10;
        }

        $ch2 = $ch;
        switch ($ch) {
        case "\\":
            if ($chars) {
                $tmp = array_shift($chars);
                if ($tmp === 'x') {
                                                            $tmp = array_shift($chars) . array_shift($chars);
                    array_push($string, chr(hexdec($tmp)));
                } elseif (preg_match("![0-7]!", $tmp)) {
                    print "octal parsing not implemented.  treating literally.\n";
                    array_push($string, $tmp);
                } elseif (@$patterns["\\$tmp"]) {
                    $ch2 .= $tmp;
                    array_push($string, $patterns[$ch2]);
                } else {
                    print "'\\$tmp' being treated as literal '$tmp'\n";
                    array_push($string, $tmp);
                }
            } else {
                print "regex not terminated";
                exit();
            }
            break;
        case '.':
            array_push($string, $patterns[$ch2]);
            break;
        case '[':
            $tmp = array();
            while (($ch2 = array_shift($chars)) !== '') {
                if ($ch2 === ']') { break; }
                if ($ch2 === '-' && $chars && $tmp) {
                    $ch2 = array_shift($chars);
                    for ($n = ord($tmp[count($tmp)-1]); $n < ord($ch2); ++$n) {
                        array_push($tmp, chr($n+1));
                    }
                } else {
                    if (preg_match("/\W/", $ch2)) {
                        print "'$ch' will be treated literally inside []\n";
                    }
                    array_push($tmp, $ch2);
                }
            }
            if ($ch2 !== ']') {
                print "unmatched []\n";
                exit();
            }
            array_push($string, $tmp);
            break;
        case '*':
            $add = preg_split("//", "{0,}", -1, PREG_SPLIT_NO_EMPTY);
            $chars = array_merge($add, $chars);
            break;
        case '+':
            $add = preg_split("//", "{1,}", -1, PREG_SPLIT_NO_EMPTY);
            $chars = array_merge($add, $chars);
            break;
        case '?':
            $add = preg_split("//", "{0,1}", -1, PREG_SPLIT_NO_EMPTY);
            $chars = array_merge($add, $chars);
            break;
        case '{':
            $closed = 0;
            for ($n = 0; $n < count($chars); ++$n) {
                  if ($chars[$n] !== "}") {
                      $closed = 1;
                      break;
                  }
            }
            if ($closed) {
                $tmp = '';
                while (($ch2 = array_shift($chars)) !== '') {
                    if ($ch2 === "}") { break; }
                    if (!preg_match("/[\d,]/", $ch2)) {
                        print "'$ch' inside {} not supported\n";
                        exit();
                    }
                    $tmp .= $ch2;
                }
                if (preg_match("/,/", $tmp)) {
                    if (preg_match("/^(\d*),(\d*)$/", $tmp, $minmax)) {
                        $min = $minmax[1] ? $minmax[1] : 0;
                        $max = $minmax[2] ? $minmax[2] : $maxCount;
                        if ($min > $max) {
                            print "bad range $tmp";
                            exit();
                        }
                        if ($min === $max) {
                            $tmp = $min;
                        } else {
                            $tmp = $min + mt_rand(0, $max - $min);
                        }
                    } else {
                        print "malformed range $tmp\n";
                        exit();
                    }
                }
                if ($tmp) {
                    $last = $string[count($string)-1];
                    for ($n=0; $n < ($tmp-1); ++$n) {
                        array_push($string, $last);
                    }
                } else {
                    array_pop($string);
                }
            } else {
                array_push($string, $ch2);
            }
            break;
        }
        return array($chars, $string);
    }

        
    /**
     * @param string  $patterns the input pattern strings
     * @return string
     * @access private
     */
    function _randpattern($patterns)
    {
        if (is_a(@$this, 'String_Random')) {
            $oldPatterns = $this->_old_patterns;
        } else {
            $oldPatterns = String_Random::_getOldPatterns();
        }

        $string = '';
        $chars = preg_split('//', $patterns, -1, PREG_SPLIT_NO_EMPTY);
        while ($ch = array_shift($chars)) {
            if (array_key_exists($ch, $oldPatterns)) {
                $string .= $oldPatterns[$ch][mt_rand(0, count($oldPatterns[$ch])-1)];
            } else {
                print "Unknown pattern character \"$ch\"!\n";
                exit();
            }
        }
        return $string;
    }

        
    /**
     * @return array
     * @access private
     */
    function _getPatterns()
    {
        $uppers = String_Random::_getUppers();
        $lowers = String_Random::_getLowers();
        $digits = String_Random::_getDigits();
        $puncts = String_Random::_getPunctuations();
        $binaries = String_Random::_getBinaries();

        $any = array_merge($uppers, $lowers, $digits, $puncts);
        $salt = array_merge($uppers, $lowers, $digits, array(".", "/"));

        $puncts2 = array();
        foreach ($puncts as $v) {
            if ($v !== '_') {
                array_push($puncts2, $v);
            }
        }

        $patterns = array(                          '.' => $any,
                          '\d' => $digits,
                          '\D' => array_merge($uppers, $lowers, $puncts),
                          '\w' => array_merge($uppers, $lowers, $digits, array("_")),
                          '\W' => $puncts2,
                          '\s' => array(" ", "\t"),
                          '\S' => array_merge($uppers, $lowers, $digits, $puncts),

                                                                              '\t' => array("\t"),
                          '\n' => array("\n"),
                          '\r' => array("\r"),
                          '\f' => array("\f"),
                          '\a' => array("\a"),
                          '\e' => array("\e"),
                          );

        return $patterns;
    }

        
    /**
     * @return array
     * @access private
     */
    function _getOldPatterns()
    {
        $uppers = String_Random::_getUppers();
        $lowers = String_Random::_getLowers();
        $digits = String_Random::_getDigits();
        $puncts = String_Random::_getPunctuations();
        $binaries = String_Random::_getBinaries();

        $any = array_merge($uppers, $lowers, $digits, $puncts);
        $salt = array_merge($uppers, $lowers, $digits, array(".", "/"));

        $oldPatterns = array('C' => $uppers,
                              'c' => $lowers,
                              'n' => $digits,
                              '!' => $puncts,
                              '.' => $any,
                              's' => $salt,
                              'b' => $binaries
                              );

        return $oldPatterns;
    }

        
    /**
     * @return array
     * @access private
     */
    function _getUppers()
    {
        $uppers = range('A', 'Z');
        return $uppers;
    }

        
    /**
     * @return array
     * @access private
     */
    function _getLowers()
    {
        $lowers = range('a', 'z');
        return $lowers;
    }

        
    /**
     * @return array
     * @access private
     */
    function _getDigits()
    {
        $digits = range('0', '9');
        return $digits;
    }

        
    /**
     * @return array
     * @access private
     */
    function _getPunctuations()
    {
        $punct = array();
        for ($i=33; $i<48; ++$i) { $punct[] = chr($i); }
        for ($i=58; $i<65; ++$i) { $punct[] = chr($i); }
        for ($i=91; $i<97; ++$i) { $punct[] = chr($i); }
        for ($i=123; $i<127; ++$i) { $punct[] = chr($i); }

        return $punct;
    }

        
    /**
     * @return array
     * @access private
     */
    function _getBinaries()
    {
        $binaries = array();
        for ($i=0; $i<256; ++$i) { $binaries[] = chr($i); }

        return $binaries;
    }

}

?>
