<?php

// Version 1.0	とりあえず作成
// Version 1.1	delete_tmpを追加
// Version 1.2	必ず chmod 0666 する

class exfileupload
{
	var $tmp_path;
	var $filelist=array();

		function exfileupload($tmp_path){
		$this->tmp_path=$tmp_path;
	}

		function move(){
		umask();
		foreach ($_FILES as $key => $value){
			if ( $_FILES[$key]['error']==UPLOAD_ERR_INI_SIZE){
				$this->filelist["up_$key"]['name']='';
				$this->filelist["up_$key"]['uploaded_name']='';
				$this->filelist["up_$key"]['uploaded_basename']='';
				$this->filelist["up_$key"]['error']="ファイルサイズがアップロード上限値".ini_get('upload_max_filesize')."Bytesを超えています。";
			}
			else if (is_uploaded_file($_FILES[$key]['tmp_name'])){
				$ext = $this->_get_ext($_FILES[$key]['type']);									if( move_uploaded_file($_FILES[$key]['tmp_name'], $this->tmp_path.'/'.basename($_FILES[$key]['tmp_name'].$ext) ) ){
					$this->filelist["up_$key"]['name']=$_FILES[$key]['name'];
					$this->filelist["up_$key"]['type']=$_FILES[$key]['type'];
					$this->filelist["up_$key"]['size']=$_FILES[$key]['size'];
					$this->filelist["up_$key"]['ext']=$ext;
					$this->filelist["up_$key"]['uploaded_name']=$this->tmp_path.'/'.basename($_FILES[$key]['tmp_name'].$ext);
					$this->filelist["up_$key"]['uploaded_basename']=basename($_FILES[$key]['tmp_name'].$ext);
					
					$chmod_flag = chmod ($this->filelist["up_$key"]['uploaded_name'], 0666);
					if (! $chmod_flag){ die('chmod が出来ませんでした'); }
					
				}
				else{
				die('[ ERROR: can not move'.$this->tmp_path.' ]');
				}
			}
		}
		return $this->filelist;
	}

		function _get_ext($type){
		if      ( preg_match('/jpeg/', $type) ){ return '.jpg'; }
		else if ( preg_match('/jpg/', $type)  ){ return '.jpg'; }
		else if ( preg_match('/gif/', $type)  ){ return '.gif'; }
		else if ( preg_match('/png/', $type)  ){ return '.png'; }
		else if ( $result=preg_replace('/(.+)\//', '', $type) ){ return '.'.$result; }
	}

		function delete_tmp($dirpath=''){

		if (!$dirpath){ return; }
		$deleted_list=array();
		$dir = dir($dirpath);
		while ( ($file=$dir->read()) !== FALSE ){
						if (preg_match('/^\./',$file)){ continue; }				else {
				$filetime=filemtime("$dirpath/$file"); $nowtime=time();
				$int_f=intval($filetime);	$int_n=intval($nowtime);
				$sa=($int_n-$int_f);							if ($sa > (60*60*24)){													array_push($deleted_list,$file );
					if ( ! unlink("$dirpath/$file") ){ die("ファイル[".$dirpath."/".$file."]の削除に失敗しました"); }
				}
			}
		}
		return $deleted_list;
	}

		function dump($data){
		mb_convert_variables('EUC', 'auto', $data);
		print "<pre>";
		print_r($data);
		print "</pre>";
	}
}
?>