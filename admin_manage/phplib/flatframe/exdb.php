<?php
require_once("DB.php"); 

/*
	exdb.php
	copyright (c)2002-2008 econosys system
	http://www.econosys.jp/system/

	Version
	0.01 ：とりあえず作成
	0.03 ：エラー時の表示方法を変更
	0.04 ：insertメソッドでnull値が0として登録されるのを修正
	0.05 ：quoteSmartするときに(string)型を指定するように変更
	0.06 ：細かなバグフィックス
	0.07 ：count_sql へ渡す sql文はselectのsql文とするように変更した
	0.08 ：find_or_create メソッドを修正
	0.09 ：select_one 時に ORDER_BY が無効だったのを修正
	0.091：insert時の now() 正規表現を /^NOW()$/i とした
	0.092：find メソッドを追加
	0.093：find メソッドを変更（成功時にidを返すように）
	0.10 ：delete_allメソッドを追加
	0.11 ：ドキュメントをすこしずつ追加
	0.111：exec_sql の戻り値を 影響を受けた行数に変更
	0.2  ：実行時間を取得するようにした
	0.201：new 時エラーの追加
	0.202：update 時にint型カラムに空文字が入っている場合はNULLに変換するようにした
	0.21 ：dump_mode を抜ける dump_mode_off()メソッドを追加
	0.211：dump_mode の時に結果セットも表示するよう変更
	0.3  ：番号を振り直す renumber メソッドを追加
	0.301：dump_mode時に出力する文字コードを設定できるよう追加
	0.31 ：プライマリキーが複数ある時の削除メソッドのバグを修正
	0.311：select_one 時のdebug_modeのバグを修正
	0.312：find_or_create 時に引数がnullだったときに falseを返すよう修正
	0.313：exec_sql でdump_mode時に結果行数を表示するよう変更
	0.32 ：searchメソッド追加。SEARCH_MODE で OR または AND を指定する
	0.33 ：RELATION 追加
	0.34 ：postgreSQL対応
	0.35 ：postgreSQL対応で色々と修正
	0.351：UPDATE時の int型 カラムの判別を修正（postgreSQL対応）
	0.355：UPDATE時の int型 カラムの判別を修正2（postgreSQL対応）
	0.356：コンストラクタにおいてdsn が配列のときと文字列のときの判別
	0.357：select_one時のエラーを修正
	0.36 ：文字列と見なす型を定義するよう追加
	0.37 ：relation has_many を追加
	0.371：relation時の ORDER_BY 追加
	0.372：select_oneメソッドにも relationをセットできるよう追加
	0.380：updateメソッド時の now() の扱いのバグを修正
	0.381：dump_mode時に 必ず左寄せに表示するよう

	メソッド一覧
	select()
	select_sql()
	select_pager()
	select_pager_sql()
	select_one()
	select_one_sql()

	search()

	exec_sql()
	count()
	count_sql()
	find()
	find_or_create()
	update()
	delete()
	delete_all()
	renumber()

	dump_mode()
	dump_mode_off()
	debug_mode()
	debug_mode_off()


	メソッド詳細
	select( $hash=array(), $start=0, $limit=5 ) $hash：select条件の配列   $start：開始するカラム   $limit：最大取得数
	select_sql( $sql, $start=0, $limit=5 )
	select_one( $hash=array() )
	select_pager_sql( $sql, $results_per_page=20, $page_no=1 )
	insert($hash)
	exec_sql( $sql )	戻り値：$result

	リレーションの取得方法（例）

	$rec_tagdata_loop=$db_rec_tagdata_dt->select(array(
	'rec_data_id' => $this->q['rec_data_id'] ,
	'RELATION'    => array(
						'flag'        => 'has_one' ,	// リレーションの関係
						'table'       => 'rec_tag_mt' ,	// リレーションテーブル
						'from_column' => 'rec_tag_id' , // リレーション元カラム
						'to_column'   => 'rec_tag_id' , // リレーション先カラム
						)
	),0,99999);


	TODO
	必ず1件しか update 出来ないメソッド update_one を作成しよう。（primary_keyのカラムの値が全て存在するかどうかをチェックする）
*/

class exdb
{
	var $db;
	var $db_type='';				var $desc=array();
	var $tablename;
	var $dump;						var $dump_encoding_to='';		var $debug;						var $_debug_message;			var $columns=array();			var $primary=array();			var $type=array();				var $char_columns=array(			'string' ,
		'blob' ,
		'text' ,
		'char' ,
		'varchar' ,
		'year' ,
		'date' ,
		'time' ,
		'datetime' ,
		'timestamp',
	);

		function exdb($dsn,$tablename,$dbDefaultCharacterSet=null){

				if ( is_array($dsn) ){ $this->db_type=$dsn['dbsyntax']; }
				else{
			if ( preg_match('/mysql/',$dsn) ){ $this->db_type='mysql'; }
			elseif ( preg_match('/pgsql/',$dsn) ){ $this->db_type='pgsql'; }
		}

		$this->tablename=$tablename;
		$this->db=DB::connect($dsn);
		if (DB::isError($this->db)) { exit($this->db->getMessage()); }
		$this->desc=$this->db->tableinfo($tablename);
		if (DB::isError($this->db)) { exit($this->db->getMessage()); }
		if (DB::isError($this->desc)) {
									$message = '<pre>now busy please reload.</pre>'."\n";				die($message);
		}

		foreach ($this->desc as $key => $value){
			array_push($this->columns,$value['name']);
			$n=$value['name'];
			$this->type[$n]=$value['type'];
			if ( preg_match('/primary_key/',$value['flags']) ){
				array_push($this->primary,$value['name']);
			}
		}
				if ( isset($dbDefaultCharacterSet) ){
			$this->db->query("SET NAMES ".$dbDefaultCharacterSet);
			if (DB::isError($this->db)) { die($db->getMessage()); }
		}
	}

		function insert($hash){
		$sql='';
		$params=array();
				$this->_check_columns($hash);

				if( strcmp($this->db_type,'mysql')==0 ){
			$sql="INSERT INTO \n    {$this->tablename} \nSET\n";
			foreach ($hash as $key => $value){
						$sql.="    ".$key."=?, \n";
								if ( preg_match('/^NOW\(\)$/i',$value) and preg_match('/date/', $this->type[$key]) ){ $value=date("Y-m-d H:i:s",strtotime("now")); }
				else if ( preg_match('/^$/',$value) ){ $value=null; }
				array_push($params,$value);
			}
			$sql=preg_replace('/, \n$/', "\n", $sql);
		}
				elseif( strcmp($this->db_type,'pgsql')==0 ){
			$sql="INSERT INTO \n    {$this->tablename} (\n";
			foreach ($hash as $key => $value){ $sql.="        ".$key.", \n"; }
			$sql=preg_replace('/, \n$/', "\n", $sql);
			$sql.="    )\n    VALUES ( \n";

			foreach ($hash as $key => $value){
								$sql.="        ?, \n";
								if ( preg_match('/^NOW\(\)$/i',$value) and preg_match('/date/', $this->type[$key]) ){ $value=date("Y-m-d H:i:s",strtotime("now")); }
				else if ( preg_match('/^$/',$value) ){ $value=null; }
				array_push($params,$value);
			}
			$sql=preg_replace('/, \n$/', "\n", $sql);
			$sql.="    )\n";
		}

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump($params);
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add($params);
		}

		$pp     = $this->db->prepare($sql);
		$result = $this->db->execute($pp, $params);

		if($this->db->isError($result)){
			$message  = 'exdb_error(insert): ' . $result->getMessage() . "\n";				$message .= 'exdb_error(insert): ' . $result->getCode() . "\n";					$message .= '<pre>'.'exdb_error: ' . $result->getUserInfo() .'</pre>'."\n";							die($message);
		}

		$sql=''; $row=array();
				if( strcmp($this->db_type,'mysql')==0 ){
			$sql='SELECT LAST_INSERT_ID() as id';
			$this->db->setFetchMode( DB_FETCHMODE_ASSOC );				$result2 =& $this->db->query($sql);
			if($this->db->isError($result2)){
				$message .= 'exdb_error(select): ' . $result2->getMessage() . "\n";						$message .= 'exdb_error(select): ' . $result2->getCode() . "\n";						$message .= 'exdb_error(select): ' . $result2->getUserInfo() . "\n";					die($message);
			}
			$result2->fetchInto($row);
		}
		




		if ($this->dump==1){
			$this->dump("Inserted id is[".$row['id']."]");
		}
		if ($this->debug==1){
			$this->debug_add("Inserted id is[".$row['id']."]");
		}
				return($row['id']);
	}

		function select_one( $hash=array() ){

		$relation_flag=0;

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

		$order_by='';
		$this->_check_columns($hash);					$sql="SELECT \n". join(',',$this->columns). "\n FROM \n{$this->tablename} \n";
		if ($hash){ $sql.=" WHERE \n"; }
		foreach ($hash as $key => $value){
			if ( preg_match('/ORDER_BY/i',$key) ){ $order_by="ORDER BY $value"; continue; }
			elseif ( preg_match('/RELATION/i',$key) ){ $relation_flag=1; $relation=$value; continue; }
									else if ( in_array($this->type[$key], $this->char_columns) ){ $value=$this->db->quoteSmart( (string)$value); }

			$sql.="    ".$key." = ".$value." AND \n";
		}
		$sql=preg_replace('/AND \n$/', "\n", $sql);
		$sql=preg_replace('/WHERE \n$/', "", $sql);			$sql .=$order_by;

		$this->db->setFetchMode( DB_FETCHMODE_ASSOC );			$result =& $this->db->limitQuery($sql, 0, 1);	
		if($this->db->isError($result)){
			$message .= 'exdb_error: ' . $result->getMessage() . "\n";				$message .= 'exdb_error: ' . $result->getCode() . "\n";					$message .= 'exdb_error: ' . $result->getUserInfo() . "\n";				$this->dump($message);
			die();
		}
		else {
			$result->fetchInto($row);
		}

				if ($relation_flag == 1){
			$from_column_name=$relation['from_column'];
			$id = $row[$from_column_name];
			$rel_hash = $this->_get_relation( $id, $relation );
			$row['relation']=$rel_hash;
		}

				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump($row);
			$this->dump("time:{$time_work}\n");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add($row);
			$this->debug_add("time:{$time_work}\n");
		}
		return $row;
	}


		function select_one_sql( $sql ){

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

		$this->db->setFetchMode( DB_FETCHMODE_ASSOC );			$result =& $this->db->limitQuery($sql, 0, 1); 	
		if($this->db->isError($result)){
			$message .= 'exdb_error: ' . $result->getMessage() . "\n";				$message .= 'exdb_error: ' . $result->getCode() . "\n";					$message .= 'exdb_error: ' . $result->getUserInfo() . "\n";				$this->dump($message);
			die();
		}
		else {
			$result->fetchInto($row);
		}
				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump($row);
			$this->dump("time:{$time_work}\n");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add($row);
			$this->debug_add("time:{$time_work}\n");
		}


		return $row;
	}

		function exec_sql( $sql ){

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

		$message='';

		$result=& $this->db->query($sql);

		if($this->db->isError($result)){
			$message .= 'exdb_error: ' . $result->getMessage() . "\n";				$message .= 'exdb_error: ' . $result->getCode() . "\n";					$message .= 'exdb_error: ' . $result->getUserInfo() . "\n";							$this->dump($message);
			die();
		}
		
		$result_row=null;
		if( $this->db_type == 'mysql' ){ $result_row=mysql_affected_rows(); }
		if( $this->db_type == 'pgsql' ){ $result_row=$this->db->affectedRows(); }

				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump($result_row);
			$this->dump("time:{$time_work}\n");

		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add($result_row);
			$this->debug_add("time:{$time_work}\n");
		}

		return $result_row;
	}

		function find($hash){

		$primary_column_name=$this->primary[0];			$row='';
		$row=$this->select_one($hash);
		if ($row){ return($row[$primary_column_name]); }		else{ return false; }								}

		function find_or_create( $hash=array() ){

				$flag=0;
		foreach ($hash as $k => $v){
			if ( $v=='' ){}
			else{ $flag=1; }
		}
		if ($flag==0){ return false; }			
		$primary_column_name=$this->primary[0];	
		$row='';
		$row=$this->select_one($hash);

		if ($row){ return($row[$primary_column_name]); }					else{ return( $this->insert($hash) ); }		}



		function search( $hash=array(), $start=0, $limit=5 ){

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

		$sql='';
		$order_by='';
		$search_mode='AND';			$loop=array();

		$this->_check_columns($hash);	
		$sql="SELECT \n". join(',',$this->columns). "\n FROM \n{$this->tablename} \n";
		if ($hash){ $sql.=" WHERE \n"; }
		
		foreach ($hash as $key => $value){
						if ( preg_match('/ORDER_BY/i',$key) ){ $order_by="ORDER BY $value"; continue; }
			elseif ( preg_match('/SEARCH_MODE/i',$key) ){ $search_mode=$value; continue; }
			elseif ($this->type[$key]=='string' or $this->type[$key]=='blob' or $this->type[$key]=='text' or $this->type[$key]=='date'){
				if ( preg_match('/^LIKE (.+)/i',$value,$r) ){ $quote_value=$this->db->quoteSmart( (string)$r[1]); $sql.="    ".$key." LIKE ".$quote_value." {$search_mode} \n"; }
				elseif ( preg_match('/^IN (.+)/i',$value,$r) ){ $quote_value=$this->db->quoteSmart( (string)$r[1]); $sql.="    ".$key." ".$quote_value." {$search_mode} \n"; }
				else{ $quote_value=$this->db->quoteSmart( (string)$value); $sql.="    ".$key." = ".$quote_value." {$search_mode} \n"; }
			}
			else{
				if ( preg_match('/^IN (.+)/i',$value,$r) ){ $sql.="    ".$key." IN  ".$r[1]." {$search_mode} \n"; }
				else{ $sql.="    ".$key." = ".$value." {$search_mode} \n"; }
			}
		}
		if ($search_mode == 'AND'){ $sql=preg_replace('/AND \n$/', "\n", $sql); }
		elseif ($search_mode == 'OR'){ $sql=preg_replace('/OR \n$/', "\n", $sql); }
		
		$sql=preg_replace('/WHERE \n$/', "", $sql);			$sql .=$order_by;

		$this->db->setFetchMode( DB_FETCHMODE_ASSOC );			$result =& $this->db->limitQuery($sql, $start, $limit);	
		if (DB::isError($result)){				$loop=array();
		}
		else {
			while ($result->fetchInto($row)) {
				array_push($loop,$row);
			}
		}

				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump($loop);
			$this->dump("start:[$start] limit:[$limit]\n");
			$this->dump("time:{$time_work}\n");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add($loop);
			$this->debug_add("start:[$start] limit:[$limit]\n");
			$this->debug_add("time:{$time_work}\n");
		}


		return $loop;
	}




		function select( $hash=array(), $start=0, $limit=5 ){

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

		$sql='';
		$order_by='';
		$loop=array();
		$relation_flag=0;
		$relation=array();

		$this->_check_columns($hash);	
		$sql="SELECT \n". join(',',$this->columns). "\n FROM \n{$this->tablename} \n";
		if ($hash){ $sql.=" WHERE \n"; }
		
		foreach ($hash as $key => $value){
						if ( preg_match('/ORDER_BY/i',$key) ){ $order_by="ORDER BY $value"; continue; }
			elseif ( preg_match('/RELATION/i',$key) ){ $relation_flag=1; $relation=$value; continue; }
									else if ( in_array($this->type[$key], $this->char_columns) ){ $value=$this->db->quoteSmart( (string)$value); }
			$sql.="    ".$key." = ".$value." AND \n";
		}
		$sql=preg_replace('/AND \n$/', "\n", $sql);
		
		$sql=preg_replace('/WHERE \n$/', "", $sql);			$sql .=$order_by;

		$this->db->setFetchMode( DB_FETCHMODE_ASSOC );			$result =& $this->db->limitQuery($sql, $start, $limit);	
		if (DB::isError($result)){				$loop=array();
		}
		else {
			while ($result->fetchInto($row)) {
				array_push($loop,$row);
			}
		}

				if ($relation_flag == 1){
			$from_column_name=$relation['from_column'];
			$i=0;
			foreach ($loop as $k => $v){
				$id=$v[$from_column_name];
				$rel_hash=$this->_get_relation( $id, $relation );
				$loop[$i]['relation']=$rel_hash;
				$i++;
			}
		}

				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump($loop);
			$this->dump("start:[$start] limit:[$limit]\n");
			$this->dump("time:{$time_work}\n");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add($loop);
			$this->debug_add("start:[$start] limit:[$limit]\n");
			$this->debug_add("time:{$time_work}\n");
		}


		return $loop;
	}

		function select_pager( $hash=array(), $results_per_page=20, $page_no=1 ){
		$start=$results_per_page * ($page_no - 1);
		return ($this->select($hash, $start, $results_per_page) );
	}

		function select_sql( $sql, $start=0, $limit=5 ){

		if ( is_array($sql) ){ die("can not use ARRAY ARGUMENTS in select_sql METHOD"); }

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

		$message='';
		$loop=array();

		if ($this->dump==1) { $this->dump($sql);      $this->dump("start:[$start] limit:[$limit]"); }
		if ($this->debug==1){ $this->debug_add($sql); $this->debug_add("start:[$start] limit:[$limit]"); }

		$this->db->setFetchMode( DB_FETCHMODE_ASSOC );	
		$result =& $this->db->limitQuery($sql, $start, $limit);

		if($this->db->isError($result)){
			$message .= 'exdb_error: ' . $result->getMessage() . "\n";				$message .= 'exdb_error: ' . $result->getCode() . "\n";					$message .= 'exdb_error: ' . $result->getUserInfo() . "\n";							$this->dump($message);
			die();
		}
		else {
			while ($result->fetchInto($row)) {
				array_push($loop,$row);
			}
		}
				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($loop);
			$this->dump("time:{$time_work}\n");
		}
		if ($this->debug==1){
			$this->debug_add($loop);
			$this->debug_add("time:{$time_work}\n");
		}

		return $loop;
	}


		function select_pager_sql( $sql, $results_per_page=20, $page_no=1 ){
		$start=$results_per_page * ($page_no - 1);
		return ($this->select_sql($sql, $start, $results_per_page) );
	}


		function count($hash=array()){

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

		$this->_check_columns($hash);	
		$sql="SELECT count(*) as count from {$this->tablename}";
		if ($hash){ $sql.=" WHERE \n"; }
		foreach ($hash as $key => $value){
			if ( $key == 'ORDER_BY' ){ $order_by="ORDER BY $value"; continue; }
									if ( in_array($this->type[$key], $this->char_columns) ){ $value=$this->db->quoteSmart( (string)$value); }

			$sql.="    ".$key." = ".$value." AND \n";
		}
		$sql=preg_replace('/AND \n$/', "\n", $sql);

		$this->db->setFetchMode( DB_FETCHMODE_ASSOC );			$result =& $this->db->limitQuery($sql, 0, 1);

		if($this->db->isError($result)){
			$message .= 'exdb_error: ' . $result->getMessage() . "\n";				$message .= 'exdb_error: ' . $result->getCode() . "\n";					$message .= 'exdb_error: ' . $result->getUserInfo() . "\n";				$this->dump($message);
			die();
		}
		$result->fetchInto($row);

				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump("count:{$row['count']}\n");
			$this->dump("time:{$time_work}\n");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add("count:{$row['count']}\n");
			$this->debug_add("time:{$time_work}\n");
		}

		return $row['count'];
	}

			function count_sql($sql){

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

		$message='';

		$this->db->setFetchMode( DB_FETCHMODE_ORDERED );			$result =& $this->db->query($sql);

		if($this->db->isError($result)){
			$message .= 'exdb_error: ' . $result->getMessage() . "\n";				$message .= 'exdb_error: ' . $result->getCode() . "\n";					$message .= 'exdb_error: ' . $result->getUserInfo() . "\n";				$this->dump($message);
			$this->dump("=====\n".$sql."\n=====\n");
			die();
		}

				$count=0;
		while ($result->fetchInto($row) ){ $count++; }

				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump("count:{$count}\n");
			$this->dump("time:{$time_work}\n");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add("count:{$count}\n");
			$this->debug_add("time:{$time_work}\n");
		}
		return $count;
	}


		function update($hash){

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

		$sql=''; $where=''; $where_flag=0;
		$params=array();
		$this->_check_columns($hash);			$sql="UPDATE {$this->tablename}\n SET \n";
		foreach ($hash as $key => $value){
						if ( preg_match('/^NOW\(\)$/i',$value) and preg_match('/date/', $this->type[$key]) ){ $value="'".date("Y-m-d H:i:s",strtotime("now"))."'"; }

									else if ( in_array($this->type[$key], $this->char_columns) ){ $value=$this->db->quoteSmart( (string)$value); }

						else if ( preg_match('/int/i',$this->type[$key]) && $value=='' ){ $value='NULL'; }				if ( in_array($key,$this->primary) ){ 				$where.="    ".$key." = ".$value." AND \n";
				$where_flag=1;
			}
			else{ $sql.="    ".$key." = ".$value." , \n"; }
		}
		$where=preg_replace('/AND \n$/', "\n", $where);
		$sql=preg_replace('/, \n$/', "\n", $sql);
		$sql=$sql."WHERE \n".$where;

		if ($where_flag==0){ die("exdb_error: primary_key のカラムを指定して下さい"); }

		$result =& $this->db->query($sql);

		if($this->db->isError($result)){
			$message = 'exdb_error: ' . $result->getMessage() . "\n";				$message .= 'exdb_error: ' . $result->getCode() . "\n";					$message .= 'exdb_error: ' . $result->getUserInfo() . "\n";							$this->dump($message);
			die();
		}

		$result_row=null;
		if( $this->db_type == 'mysql' ){ $result_row=mysql_affected_rows(); }
		if( $this->db_type == 'pgsql' ){ $result_row=$this->db->affectedRows(); }


				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump("[$result_row]Row(s) updated");
			$this->dump("time:{$time_work}\n");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add("[$result_row]Row(s) updated");
			$this->debug_add("time:{$time_work}\n");
		}

		return $result_row;
	}

		function delete($hash){
		$message='';
		$sql=''; $where='';
		$params=array();
		$this->_check_columns($hash);			$sql="delete FROM {$this->tablename}\n WHERE \n";
		foreach ($hash as $key => $value){
			$sql.="    ".$key."=? AND \n";
			array_push($params,$value);
		}
		$sql=preg_replace('/AND \n$/', "\n", $sql);	
		$pp     = $this->db->prepare($sql);
		$result = $this->db->execute($pp, $params);

		if($this->db->isError($result)){
			$message .= 'exdb_error: ' . $result->getMessage() . "\n";				$message .= 'exdb_error: ' . $result->getCode() . "\n";					$message .= 'exdb_error: ' . $result->getUserInfo() . "\n";				$this->dump($message);
			$this->dump("=====\n".$sql."\n=====\n");
			die();
		}
		$result_row=null;
		if( $this->db_type == 'mysql' ){ $result_row=mysql_affected_rows(); }
		if( $this->db_type == 'pgsql' ){ $result_row=$this->db->affectedRows(); }

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump($params);
			$this->dump("[$result_row]Row(s) deleted");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add($params);
			$this->debug_add("[$result_row]Row(s) deleted");
		}
		return $result_row;
	}

		function delete_all(){

				$sql="DELETE FROM {$this->tablename} \n";

		$pp     = $this->db->prepare($sql);
		$result = $this->db->execute($pp);

		if($this->db->isError($result)){
			$message .= 'exdb_error: ' . $result->getMessage() . "\n";				$message .= 'exdb_error: ' . $result->getCode() . "\n";					$message .= 'exdb_error: ' . $result->getUserInfo() . "\n";				$this->dump($message);
			$this->dump("=====\n".$sql."\n=====\n");
			die();
		}
		$result_row=null;
		if( $this->db_type == 'mysql' ){ $result_row=mysql_affected_rows(); }
		if( $this->db_type == 'pgsql' ){ $result_row=$this->db->affectedRows(); }

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump($params);
			$this->dump("[$result_row]Row(s) deleted");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add($params);
			$this->debug_add("[$result_row]Row(s) deleted");
		}

				$sql="ALTER TABLE {$this->tablename} PACK_KEYS =0 CHECKSUM =0 DELAY_KEY_WRITE =0 AUTO_INCREMENT =1";
		$pp     = $this->db->prepare($sql);
		$result = $this->db->execute($pp);

		return $result_row;
	}

		function renumber($column_name, $sort_start, $sort_countup){

				$time_start=0; $time_end=0; $time_work=0;
		$time_start=$this->_getmicrotime();

				$sql="SELECT ";
		foreach ($this->primary as $k => $v){
			$sql .= "{$v}, ";						}
		$sql .= "{$column_name} FROM {$this->tablename} ORDER BY {$column_name}";
		$start=0; $limit=99999;
		$list=$this->select_sql($sql, $start, $limit);

				$no=$sort_start;
		foreach ($list as $k => $v){
			$hash=array();
						foreach ($this->primary as $kkk => $vvv){
				$hash[$vvv]=$v[$vvv];
			}
			$hash[$column_name]=$no;
			$no += $sort_countup;
			$this->update($hash);
		}

				$time_end=$this->_getmicrotime();
		$time_work=$time_end - $time_start;

		if ($this->dump==1){
			$this->dump($sql);
			$this->dump("[$result_row]Row(s) updated");
			$this->dump("time:{$time_work}\n");
		}
		if ($this->debug==1){
			$this->debug_add($sql);
			$this->debug_add("[$result_row]Row(s) updated");
			$this->debug_add("time:{$time_work}\n");
		}
		return;
	}


		function _get_relation( $id=null, $relation=array() ){


		if ( $id==null ){ die("PLEASE SET relation_id"); }
		elseif( count($relation) < 4 ){ die("PLEASE SET relation"); }

		$db_relation=new exdb($this->db->dsn,$relation['table']);

				if ( strcmp($relation['flag'],'has_one')==0 ){
			$hash=$db_relation->select_one(array(
				$relation['to_column'] => $id ,
			));
			return $hash;
		}
				elseif ( strcmp($relation['flag'],'has_many')==0 ){
			$qhash=array(
				$relation['to_column'] => $id ,
			);
						if ( isset($relation['ORDER_BY']) ){ $qhash['ORDER_BY']=$relation['ORDER_BY']; }
			$loop=$db_relation->select($qhash,0,999999);
			return $loop;
			
		}

	}


		function _check_columns($hash){
		foreach ($hash as $key => $value){
			if ( $key == 'ORDER_BY' ){ continue; }
			if ( $key == 'SEARCH_MODE' ){ continue; }
			if ( $key == 'RELATION' ){ continue; }
			if (! in_array($key,$this->columns) ){ die("column '$key' is not find in table '{$this->tablename}'"); }
		}
	}


		function dump($data){

		if ( $this->dump_encoding_to != '' ){
			mb_convert_variables( $this->dump_encoding_to, 'auto', $data );
		}

		print "\n".'<pre style="text-align:left;">'."\n";
		print "==============================\n";
		print_r($data);
				print "</pre>";
	}

		function dump_mode($dump_encoding_to=''){
		if ($dump_encoding_to != ''){ $this->dump_encoding_to=$dump_encoding_to; }
		$this->dump=1;
	}

		function dump_mode_off(){
		$this->dump=0;
	}


		function debug_mode(){
		$this->debug=1;
	}

		function debug_mode_off(){
		$this->debug=0;
	}

		function debug_print(){
		return $this->_debug_message;
	}

		function _getmicrotime(){
		list($msec, $sec) = explode(" ", microtime());
		return ((float)$sec + (float)$msec);
	}

		function debug_add($data){
		$this->_debug_message .= sprintf ("<pre>");
		$this->_debug_message .= sprintf ("==============================\n");
		$this->_debug_message .= print_r($data,TRUE);
				$this->_debug_message .= sprintf ("</pre>");
	}
}
?>