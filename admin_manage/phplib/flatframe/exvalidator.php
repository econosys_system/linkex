<?php

/*
	exvalidator.php
	copyright (c)2002-2008 econosys system
	http://www.econosys.jp/system/

	Version
	0.01 ：とりあえず作成
	0.02 ：ソースを整形
	0.03 ：E-MAIL検知方法、 not blank 検知方法を修正
	
*/

class validator
{
	var $q                = array();
	var $validator_config = array();
	var $result           = array();

		function validator($yamlfile, $q){
		$this->validator_config = Spyc::YAMLLoad( "$yamlfile" );
		$this->q = $q;
	}

		function check($group){
		$validator_group=array();
		if (array_key_exists($group, $this->validator_config)) {
			$validator_group=$this->validator_config[$group];
		}
		else{
			die('[ error : '.__CLASS__.' ]'."設定ファイル（YAML）内にvalidator_groupの".$group."をセットして下さい");
		}
				foreach ($validator_group as $itemname => $value){

			if (array_key_exists($itemname, $this->q)) {
				foreach ($value as $k => $err_message){
					     if ($k=='NOT_BLANK')       { $this->validation_not_blank($this->q[$itemname], $itemname, $err_message); }
					else if ($k=='NOT_BLANK_SELECT'){ $this->validation_not_blank_select($this->q[$itemname], $itemname, $err_message); }
					else if ($k=='EMAIL')        { $this->validation_email($this->q[$itemname], $itemname, $err_message); }
					else if ( ereg('^LENGTH',$k )){
						list($dummy,$min,$max) = split(",", $k);
						$this->validation_length($this->q[$itemname], $min, $max, $itemname, $err_message);
					}
					else if ( ereg('^DUPLICATION',$k ) ){
						list($dummy,$confirm_itemname) = split(",", $k);
						$this->validation_duplication($this->q[$itemname], $this->q[$confirm_itemname], $itemname, $err_message);
					}
					else if ( ereg('^REGEX',$k )){
						list($dummy,$pattern) = split(",", $k);
						$this->validation_regex($this->q[$itemname], $pattern, $itemname, $err_message);
					}
					else{
						die('[ error : '.__CLASS__.' ]'."未知の指定 [$k] です。設定ファイル（YAML）の書式が間違っている可能性があります");
					}
				}
			}
			else{
				die('[ error : '.__CLASS__.' ]'."フォーム項目 [$itemname] がありません");
			}
		}
		return $this->result;
	}
	
		function validation_not_blank($formvalue, $itemname, $err_message){
		if ( strcmp($formvalue,'')==0 ){
			if (! isset($this->result[$itemname]) ){ $this->result[$itemname]=$err_message; }
		}
	}
		function validation_not_blank_select($formvalue, $itemname, $err_message){
		if ( strcmp($formvalue,'')==0 || strcmp($formvalue,'none')==0){
			if (! isset($this->result[$itemname]) ){ $this->result[$itemname]=$err_message; }
		}
	}
		function validation_email($formvalue, $itemname, $err_message){
				if (! preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $formvalue)){
			if (! isset($this->result[$itemname]) ){ $this->result[$itemname]=$err_message; }
		}
	}
		function validation_length($formvalue, $min, $max, $itemname, $err_message){
		$length=strlen($formvalue);
		if ( ($length < $min) or ($length > $max) ){
			if (! isset($this->result[$itemname]) ){ $this->result[$itemname]=$err_message; }
		}
	}
		function validation_duplication($formvalue, $confirm_formvalue, $itemname, $err_message){
		if ( strcmp($formvalue,$confirm_formvalue)!=0 ){
			if (! isset($this->result[$itemname]) ){ $this->result[$itemname]=$err_message; }
		}
	}
		function validation_regex($formvalue, $pattern, $itemname, $err_message){
		if (! mb_ereg($pattern,$formvalue,$ary) ){
			if (! isset($this->result[$itemname]) ){ $this->result[$itemname]=$err_message; }
		}
	}
		function has_error(){
		if (count($this->result)){ return TRUE; }
		else { return FALSE; }
	}
}
?>