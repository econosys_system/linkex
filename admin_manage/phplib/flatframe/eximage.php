<?php

// Version 1.0	とりあえず作成
// Version 1.1	必ず chmod 0666 するように
// Version 1.2	コンストラクタで repng2jpeg を指定できるように。（デフォルトは netpbm ）
// Version 1.21	Mac用のパス /sw/bin を自動で追加するように




class eximage
{

	var $resize_program = '';

		function eximage( $resize_program_name='' ){
		if ( strcmp($resize_program_name,'repng2jpeg')==0 ){
			$this->resize_program = 'repng2jpeg';
		}
		else{
			$this->resize_program = 'netpbm';
		}
		
	}

		function resize($in, $out, $width='', $height='', $jpegq=100){
		$out_tmp=$out.'_resize_temporary';
		if (! $in){ die("no input file argument"); }
		else if (! file_exists($in)){ die("no input file [$in]"); }
		else if (! $out){ die("no output file argument"); }
		else if ( (!$width) and (!$height) ){ die("[eximage-error] : no width or height argument"); }

				if ( strcmp($this->resize_program,'repng2jpeg') ==0 ){
			$dir = dirname(__FILE__);
						if ($width && $height){ $exec_command = "$dir/repng2jpeg $in $out_tmp $width $height $jpegq"; }
			else if ($width)      { $height = $this->_get_size_auto($in,$width);  $exec_command = "$dir/repng2jpeg $in $out_tmp $width $height $jpegq"; }
			else if ($height)     { $width  = $this->_get_size_auto($in,$height);  $exec_command = "$dir/repng2jpeg $in $out_tmp $width $height $jpegq"; }
			else{ die("eximage: plase set width or height."); }
		}
				else{
			$path=getenv('PATH');
			$path='PATH='.$path.':/usr/bin:/usr/local/bin:/sw/bin';				putenv($path);
	
			$rt=''; $command=''; $exec_command='';
			if (preg_match('/.jpg$/',$in)){ $command='djpeg'; }
			else if (preg_match('/.gif$/',$in)){ $command='giftopnm'; }
			else if (preg_match('/.png$/',$in)){ $command='pngtopnm'; }
	
			if ($width && $height){ $exec_command="$command $in | pnmscale -xsize $width -ysize $height | cjpeg -quality $jpegq > $out_tmp"; }
			else if ($width)      { $exec_command="$command $in | pnmscale -xsize $width | cjpeg -quality $jpegq > $out_tmp"; }
			else if ($height)     { $exec_command="$command $in | pnmscale -ysize $height | cjpeg -quality $jpegq > $out_tmp"; }
			else{ die("eximage: plase set width or height."); }
		}
		
		system("$exec_command", $rt);
		rename($out_tmp, $out);
		
		$chmod_flag = chmod ($out, 0666);
		if (! $chmod_flag){ die('chmod が出来ませんでした'); }

		if ($rt==0){ return TRUE; }
		else { die("can not resize [$exec_command]"); }
	}

	function _get_size_auto( $file='', $resize_width=0 ){
		list($width,$height) = getimagesize($file);
		$resize_height = round( ($resize_width/$width) * $height );
		return $resize_height;
	}

}
?>