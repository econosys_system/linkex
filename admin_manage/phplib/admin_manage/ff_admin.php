<?php
// Version 1.00  ：公開
// Version 1.01  ：リンク変換 .php ファイルが存在しなかった時エラーとなるのを修正
// Version 1.02  ：bug-fix

require_once './phplib/flatframe.php';
require_once './phplib/flatframe/textdb.php';
require_once './phplib/flatframe/FillInForm.class.php';
require_once './phplib/flatframe/exminimalauth.php';

class ff_topics_admin extends flatframe
{
    public $config = array();
    public $config_admin = array();
    public $breadcrums_loop = array();
    function ff_topics_admin($configfile)
    {
        $this->_ff_configfile = $configfile;
    }
    function setup()
    {
        $this->rootdir = dirname(__FILE__);
        $this->run_modes = array(
            'default'                => 'do_index',
            'index'                  => 'do_index',
            'search'                 => 'do_search',
            'add'                    => 'do_add',
            'add_confirm'            => 'do_add_confirm',
            'add_submit'             => 'do_add_submit',
            'edit'                   => 'do_edit',
            'edit_submit'            => 'do_edit_submit',
            'delete_confirm'         => 'do_delete_confirm',
            'delete_submit'          => 'do_delete_submit',
            'submit_redirect'        => 'do_submit_redirect',
            'admin_tmpl_edit'        => 'do_admin_tmpl_edit',
            'admin_tmpl_edit_submit' => 'do_admin_tmpl_edit_submit',
            'count_reset_confirm'    => 'do_count_reset_confirm',
            'count_reset_submit'     => 'do_count_reset_submit',
            'csv_download'           => 'do_csv_download',
            'csv_download_submit'    => 'do_csv_download_submit',
            'convert'                => 'do_convert',
            'convert_confirm'        => 'do_convert_confirm',
            'convert_submit'         => 'do_convert_submit',
            'html_preview'           => 'do_html_preview',
         );

    }
    function app_prerun()
    {
        ini_set("max_execution_time",1800); // タイムアウトを1800秒にセット
        $back_url = (empty($_SERVER['HTTPS']) ? 'http://' : 'https://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $back_url = $_SERVER['REQUEST_URI'];
        $this->assign(array('back_url' => $back_url));
        ini_set('session.cookie_lifetime', 28800);
        session_cache_expire(480);
        ini_set('log_errors', 1);

        $ma = new exminimalauth( array(
          'admin_password' => $this->_ff_config['admin_password'] ,
        ));
        $ma->login( "{$this->q['_program_uri']}?cmd=login_submit", (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] );
    }
    function app_postrun()
    {
    }

    function login_submit(){
    }

    function assigns()
    {
        $this->assign($this->config);
        $this->assign($this->config_admin);
        $this->assign($this->q);
// $this->dump( $this->q );
        $this->assign(array('breadcrums_loop' => $this->breadcrums_loop));
        if (isset($_SESSION['admin_name'])) {
            $this->assign(array('admin_name' => $_SESSION['admin_name']));
        }
    }



    function _write_public_pages()
    {
        $admin_dt = new textdb("{$this->rootdir}/textdb.yml", 'admin_dt', $this->_ff_config['data_dir']);
        $admin_loop = $admin_dt->select(array('admin_id' => 1), 0, 1);
        $admin_hash = $admin_loop[0];
// $this->dump( $admin_hash  );
        $this->assign(array('admin_hash' => $admin_hash));

        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
        $data_loop = $data_dt->select(array(), 0, 99999);
        $this->assigns();
        //			$this->assign( array('data_loop' => $data_loop) );

        // 全ファイル削除
        $this->delete_allfile($this->_ff_config['public_html_dir']);

        foreach ($data_loop as $k => $v) {
            // $this->dump( $v );
            $detail_filename = "{$this->_ff_config['public_html_dir']}/{$v['data_name']}.php";
            $this->assign(array('hash' => $v));
            $html = $this->template->fetch('public_php.php');
            //					$this->assign( $v );
            $this->_write_page($detail_filename, $html, $this->_ff_config['tmp_dir']);
        }
        // $html = $this->template->fetch("public_html.html");
        // $this->_write_page( $this->_ff_config['public_toppage'], $html, $this->_ff_config['upload_tmp_dir'] );
        // $html = $this->template->fetch("public_topics.html");
        // $this->_write_page( $this->_ff_config['public_topics'], $html , $this->_ff_config['upload_tmp_dir'] );
        // $this->delete_allfile($this->_ff_config['topics_dir']);
    }



    function _write_page($file_name, $html, $tmp_dir)
    {
        $pid = getmypid();
        $tmp_filename = "{$tmp_dir}/".$pid.time();
        $filename = $file_name;
        $fptmp = fopen($tmp_filename, 'w') or die('textdb:error cannot open temporary file');
        fwrite($fptmp, $html);
        fclose($fptmp);
        $this->_move($tmp_filename, $filename);
        if ( isset($this->_ff_config['public_page_permission']) ){
          $d = octdec($this->_ff_config['public_page_permission']); // 10進数に変換
          // OFF $o = decoct($d);
          chmod($filename, $d);
        }
    }



    // exdb を使用した今日のアクセス数カウント（遅い）
    function ___old___count_today_exdb($data_id)
    {
        require_once './phplib/flatframe/exdate.php';
        $t = new exdate();
        list($year, $month, $day, $hour, $min, $sec) = $t->now();
        $today_text = "{$year}/{$month}/{$day}";

        $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);
        $data_loop = $count_dt->select(array('data_id' => $data_id), 0, 9999);
        $count = 0;
        foreach ($data_loop as $k => $v) {
            if (strpos($v['access_date'], $today_text) !== false) {
                ++$count;
            }
            else if (1){}
        }
        return $count;
    }



    function _count_today($data_id)
    {
      // $this->dump( $data_id );
        //exdate
        require_once './phplib/flatframe/exdate.php';
        $z = new exdate();
        list($year, $month, $day, $hour, $min, $sec) = $z->now();
        $t = new exdate( "{$year}-{$month}-{$day} 00:00:00" );
        $today_text = "{$year}/{$month}/{$day}";

        //exdb
        $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);

        $filename = $this->_ff_config['data_dir'].'/count_dt.cgi';
        $fp = fopen($filename,'r') or die("ファイル({$filename})のオープンに失敗しました");
        $count = 0;
        while ( ($line = fgets($fp,99999)) !== false ) { // 1行あたり最大99999bytes
            $h = $count_dt->_read_data($line);
// $this->dump( $h ); die;
            if (strpos($h['access_date'], $today_text) !== false) {
              if ( (int)$h['data_id'] === (int)$data_id ){
                ++$count;
              }
            }
            else{
              preg_match("/^([0-9]{4})\/([0-9]{2})\/([0-9]{2})/",$h['access_date'], $r);
              $tmp_t = new exdate($r[1], $r[2], $r[3] );
              // $this->dump( $h );
              // $this->dump( $r );
              // $this->dump( $t->timestamp_now().':'.$tmp_t->timestamp_now() ); die;
              if( $t->timestamp_now() > $tmp_t->timestamp_now() ){
                break;
              }
            }
        }
        fclose($fp);
        return $count;
    }




    function _count_this_month($data_id)
    {
      // $this->dump( $data_id );
        //exdate
        require_once './phplib/flatframe/exdate.php';
        $z = new exdate();
        list($year, $month, $day, $hour, $min, $sec) = $z->now();
        $t = new exdate( "{$year}-{$month}-{$day} 00:00:00" );
        $today_text = "{$year}/{$month}";

        //exdb
        $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);

        $filename = $this->_ff_config['data_dir'].'/count_dt.cgi';
        $fp = fopen($filename,'r') or die("ファイル({$filename})のオープンに失敗しました");
        $count = 0;
        while ( ($line = fgets($fp,99999)) !== false ) { // 1行あたり最大99999bytes
            $h = $count_dt->_read_data($line);
// $this->dump( $h ); die;
            if (strpos($h['access_date'], $today_text) !== false) {
              if ( (int)$h['data_id'] === (int)$data_id ){
                ++$count;
              }
            }
            else{
              preg_match("/^([0-9]{4})\/([0-9]{2})\/([0-9]{2})/",$h['access_date'], $r);
              $tmp_t = new exdate($r[1], $r[2], $r[3] );
              // $this->dump( $h );
              // $this->dump( $r );
              // $this->dump( $t->timestamp_now().':'.$tmp_t->timestamp_now() ); die;
              if( $t->timestamp_now() > $tmp_t->timestamp_now() ){
                break;
              }
            }
        }
        fclose($fp);
        return $count;
    }



    function _count_per_month($data_id)
    {
      // $this->dump( $data_id );
        //exdate
        require_once './phplib/flatframe/exdate.php';
        $z = new exdate();
        list($year, $month, $day, $hour, $min, $sec) = $z->now();
        $t = new exdate( "{$year}-{$month}-{$day} 00:00:00" );
        $today_text = "{$year}/{$month}";

        //exdb
        $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);
        $c_loop = $count_dt->select(array(
          'data_id' =>$data_id ,
        ),0,999999);
        $start = $c_loop[0];
        $end_tmp = array_reverse($c_loop);
        $end     = $end_tmp[0];

// $this->dump( $start ); $this->dump( $end );

        // $this->dump( $c_loop );
        $t_start = new exdate($start['access_date']);
        $t_end   = new exdate($end['access_date']);

        // $per_month_loop の作成
        $per_month_loop = array();
        $t_start->set_next_month(); // 1つ進めておく
        $start_timestamp = $t_start->timestamp_now();
        $end_timestamp   = $t_end->timestamp_now();
// $this->dump( $start_timestamp ); $this->dump( $end_timestamp );
        while( $start_timestamp > $end_timestamp ){
            $t_start->set_prev_month();
            list($year, $month, $day) = $t_start->today();
            if (! isset($per_month_loop["{$year}_{$month}"]) ){
              $per_month_loop["{$year}_{$month}"] = 0;
            }
            $start_timestamp = $t_start->timestamp_now();
// $this->dump( $start_timestamp );
        }
// $this->dump( $per_month_loop );


        $count = 0;
        foreach ($c_loop as $h) {
// $this->dump( $h );
            preg_match("/^([0-9]{4})\/([0-9]{2})\/([0-9]{2})/",$h['access_date'], $r);
            $year = $r[1]; $month = $r[2]; $day = $r[3];
            if ( isset($per_month_loop["{$year}_{$month}"]) ){
                $per_month_loop["{$year}_{$month}"]++;
            }
            else{
              $per_month_loop["{$year}_{$month}"] = 1;
            }
            ++$count;
        }
// $this->dump( $per_month_loop );
      return $per_month_loop;
    }




    function _count_all($data_id)
    {
        //exdb
        $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);
        $ccc = $count_dt->count(array(
          'data_id' => $data_id
        ));
        // $this->dump( $ccc );
        return $ccc;
    }




    function ___old___count_this_month($data_id)
    {
        require_once './phplib/flatframe/exdate.php';
        $t = new exdate();
        list($year, $month, $day, $hour, $min, $sec) = $t->now();
        $today_text = "{$year}/{$month}/";
        $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);
        $data_loop = $count_dt->select(array('data_id' => $data_id), 0, 9999);
        $count = 0;
        foreach ($data_loop as $k => $v) {
            if (strpos($v['access_date'], $today_text) !== false) {
                ++$count;
            }
        }

        return $count;
    }



    function _count_reset($data_id=0)
    {
        require_once './phplib/flatframe/exdate.php';
        $t = new exdate();
        list($year, $month, $day, $hour, $min, $sec) = $t->now();
        $today_text = "{$year}/{$month}/{$day}";
        $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);
// $this->dump( $data_id );
// $count_dt->dump_mode();
        $result = $count_dt->delete(array(
          'data_id' => $data_id
        ));
        return $result;
    }



    function do_index()
    {
        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
        $data_loop = $data_dt->select(array(), 0, 99999);
        // $this->dump( $data_loop );
        $c = 0;
        foreach ($data_loop as $k => $v) {
            $data_loop[$c]['_jump_url'] = $this->_make_uri($this->q['_program_uri'], "../t/{$v['data_name']}.php");
            $data_loop[$c]['_count_today'] = $this->_count_today($v['data_id']);
            $data_loop[$c]['_count_this_month'] = $this->_count_this_month($v['data_id']);
            ++$c;
        }
        // CRUD用テーブル定義を読みこみassign
        $crud = new textdb("{$this->rootdir}/textdb_crud.yml", 'data_dt', $this->_ff_config['data_dir']);
        $this->assign(array('db_desc' => $crud->_config['data_dt']['desc']));

        $this->assigns();
        $this->assign(array('data_loop' => $data_loop));
        $this->template->display('admin_index.html');
    }



    function do_search()
    {
// $this->dump( $this->q );
      if ( ! @$this->q['q'] ){
          $this->do_index();
          exit();

      }

      $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
// $data_dt->help();
      $search_loop = $data_dt->or_search(array(
        'data_name'	     => $this->q['q'] ,
        'url_name'	     => $this->q['q'] ,
        'title_name'	   => $this->q['q'] ,
        'analyzer_name'	 => $this->q['q'] ,
      ),0,99999);
// $this->dump( $search_loop );

      $c = 0;
      foreach ($search_loop as $k => $v) {
          $search_loop[$c]['_jump_url'] = $this->_make_uri($this->q['_program_uri'], "../t/{$v['data_name']}.php");
          $search_loop[$c]['_count_today'] = $this->_count_today($v['data_id']);
          $search_loop[$c]['_count_this_month'] = $this->_count_this_month($v['data_id']);
          ++$c;
      }

      // CRUD用テーブル定義を読みこみassign
      $crud = new textdb("{$this->rootdir}/textdb_crud.yml", 'data_dt', $this->_ff_config['data_dir']);
      $this->assign(array('db_desc' => $crud->_config['data_dt']['desc']));

      $this->assigns();
      $this->assign(array('data_loop' => $search_loop));
      $this->template->display('admin_index.html');
    }



    function do_add()
    {
        $this->assigns();
        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
        // $this->dump( $data_dt->_config['data_dt']['desc'] );
        $this->assign(array('db_desc' => $data_dt->_config['data_dt']['desc']));
        $this->template->display('admin_edit.html');
    }



    function _check_data_name( $data_name, $data_id=false )
    {
      // ファイル名は 英数字と - _
      if (! preg_match("/^[a-zA-Z0-9_-]+$/",$data_name) ){
        return array(false,"データ登録エラー", "<b>ファイル名</b>に登録できない文字が使用されています。");
      }

      // すでに同じ名前で登録のある場合はエラー
      $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
      $d_loop = $data_dt->select(array(
        'data_name' => $this->q['data_name'],
      ),0,99999);
//$this->dump( $d_loop );
      $e_loop = array();
      foreach ($d_loop as $key => $value) {
          if ($data_id){
            if ( (int)$value['data_id'] == (int)$data_id ){}
            else{ array_push($e_loop, $value); }
          }
      }
//$this->dump( $e_loop );

      if ( count($e_loop)>=1 ){
        return array(false,"データ登録エラー", "<b>ファイル名</b>に重複する値「{$this->q['data_name']}」はつけられません。");
      }

      return array(true, null, null);
    }






    function do_add_submit()
    {
        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
        // $this->dump( $data_dt->_config['data_dt']['desc'] );
        // $this->dump( $this->q );

        list($flag,$title,$message) = $this->_check_data_name($this->q['data_name']);
        if ( ! $flag ){
          $this->assign(array('title'   => $title ));
          $this->assign(array('message' => $message ));
          $this->template->display('admin_error.html');
          exit();
        }

        // ファイル名 ( data_name )	 の重複があるときはエラー
        $d_loop = $data_dt->select(array(
          'data_name' => $this->q['data_name'],
        ),0,1);
        if ( count($d_loop)==1 ){
          $this->assign(array('title'   => "データ登録エラー"));
          $this->assign(array('message' => "<b>ファイル名</b>に重複する値「{$this->q['data_name']}」はつけられません。"));
          $this->template->display('admin_error.html');
          exit();
        }

        $insert_hash = array();
        foreach ($data_dt->_config['data_dt']['desc'] as $k => $v) {
            $column_name = $v[0];
            if (isset($this->q[$column_name])) {
                $insert_hash[$column_name] = $this->q[$column_name];
            }
        }
        $result_id = $data_dt->insert($insert_hash);
        $this->_write_public_pages();
        $this->assigns();
        header("Location: {$this->q['_program_uri']}?cmd=index");
    }



    function do_edit()
    {
        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
        $data_loop = $data_dt->select(array('data_id' => $this->q['data_id']), 0, 1);
        $hash = $data_loop[0];
        if (!isset($hash['data_id'])) {
            die('ERROR:データが取得できません');
        }
        $this->assigns();
        $this->assign(array('hash' => $hash));
        $hidden = $this->make_hidden();
        $this->assign(array('hidden' => $hidden));
        $this->assign(array('config' => $this->_ff_config));
        $this->assign(array('db_desc' => $data_dt->_config['data_dt']['desc']));
        $html = $this->template->fetch('admin_edit.html');
        $fill = new HTML_FillInForm();
        $output = $fill->fill(array('scalar' => $html, 'fdat' => $hash));
        echo $output;
    }



    function do_edit_submit()
    {
        // data_nameのチェック
        list($flag,$title,$message) = $this->_check_data_name( $this->q['data_name'], $this->q['data_id'] );
        if ( ! $flag ){
          $this->assign(array('title'   => $title ));
          $this->assign(array('message' => $message ));
          $this->template->display('admin_error.html');
          exit();
        }

        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
        $insert_hash = array();
        foreach ($data_dt->_config['data_dt']['desc'] as $k => $v) {
            $column_name = $v[0];
            if (isset($this->q[$column_name])) {
                $insert_hash[$column_name] = $this->q[$column_name];
            }
        }
        // $this->dump( $insert_hash );
        $insert_hash['data_id'] = $this->q['data_id'];
        $result_id = $data_dt->update($insert_hash);
        $this->_write_public_pages();
        header("Location: {$this->q['_program_uri']}?cmd=index");
    }



    function do_delete_confirm()
    {
        // CRUDのテーブル定義を読み込む
        $crud_dt = new textdb("{$this->rootdir}/textdb_crud.yml", 'data_dt', $this->_ff_config['data_dir']);
        $org_db_desc = $crud_dt->_config['data_dt']['desc'];
        $new_db_desc = array();
        foreach ($org_db_desc as $k => $v) {
          if ( preg_match("/^_/",$v[0]) ){}
          else{ array_push($new_db_desc,$v); }
        }
        $this->assign(array('db_desc' => $new_db_desc ));
// $this->dump( $new_db_desc );

        // data_dt
        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
        $data_loop = $data_dt->select(array('data_id' => $this->q['data_id']), 0, 1);
        $hash = $data_loop[0];
        $hidden = $this->make_hidden();
        $this->assign(array('hidden' => $hidden));
        $this->assign(array('hash' => $hash));
        $this->assigns();
        $this->template->display('admin_delete.html');
    }



    function do_delete_submit()
    {
        // data_dt
        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
        $data_delete_sum = $data_dt->delete(array('data_id' => $this->q['data_id']) );

        $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);
        $count_delete_sum = $count_dt->delete(array('data_id' => $this->q['data_id']) );

        $this->_write_public_pages();
        $this->assigns();
        header("Location: {$this->q['_program_uri']}?cmd=index");
    }



    function do_count_reset_confirm()
    {
        // CRUDのテーブル定義を読み込む
        $crud_dt = new textdb("{$this->rootdir}/textdb_crud.yml", 'data_dt', $this->_ff_config['data_dir']);
        $org_db_desc = $crud_dt->_config['data_dt']['desc'];
        $new_db_desc = array();
        foreach ($org_db_desc as $k => $v) {
          if ( preg_match("/^_/",$v[0]) ){}
          else{ array_push($new_db_desc,$v); }
        }
        $this->assign(array('db_desc' => $new_db_desc ));
// $this->dump( $new_db_desc );

        // data_dt
        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
        $data_loop = $data_dt->select(array('data_id' => $this->q['data_id']), 0, 1);
        $hash = $data_loop[0];

        $this->q['count_all']        = $this->_count_all($this->q['data_id']);
        $this->q['count_today']      = $this->_count_today($this->q['data_id']);
        $this->q['count_this_month'] = $this->_count_this_month($this->q['data_id']);

        $hidden = $this->make_hidden();
        $this->assign(array('hidden' => $hidden));
        $this->assign(array('hash' => $hash));
        $this->assigns();
        $this->template->display('admin_count_reset.html');
    }



    function do_count_reset_submit()
    {
      $result = $this->_count_reset($this->q['data_id']);
      // $this->dump( $result );

      $this->_write_public_pages();
      header("Location: {$this->q['_program_uri']}?cmd=index");
    }



    function do_csv_download()
    {
      // CRUDのテーブル定義を読み込む
      $crud_dt = new textdb("{$this->rootdir}/textdb_crud.yml", 'data_dt', $this->_ff_config['data_dir']);
      $org_db_desc = $crud_dt->_config['data_dt']['desc'];
      $new_db_desc = array();
      foreach ($org_db_desc as $k => $v) {
        if ( preg_match("/^_/",$v[0]) ){}
        else{ array_push($new_db_desc,$v); }
      }
      $this->assign(array('db_desc' => $new_db_desc ));
// $this->dump( $new_db_desc );

      // data_dt
      $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
      $data_loop = $data_dt->select(array('data_id' => $this->q['data_id']), 0, 1);
      $hash = $data_loop[0];

      $this->q['count_all']       = $this->_count_all($this->q['data_id']);
      $this->q['count_per_month'] = $this->_count_per_month($this->q['data_id']);
// $this->dump( $this->q['count_per_month'] );
      // $this->q['count_today']      = $this->_count_today($this->q['data_id']);
      // $this->q['count_this_month'] = $this->_count_this_month($this->q['data_id']);

      $hidden = $this->make_hidden();
      $this->assign(array('hidden' => $hidden));
      $this->assign(array('hash' => $hash));
      $this->assigns();
      $this->template->display('admin_csv_download.html');
    }



    function do_csv_download_submit()
    {
      // $this->dump( $this->q );

      $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
      $data_loop = $data_dt->select(array(
        'data_id' =>$this->q['data_id'] ,
      ),0,1);
      $data_hash = $data_loop[0];
// $this->dump( $data_hash );

      list($start_year,$start_month) = preg_split( "/_/",$this->q['start_date'] );
      list($end_year,$end_month)     = preg_split( "/_/",$this->q['end_date'] );

      require_once './phplib/flatframe/exdate.php';
      $t_start = new exdate( $start_year,$start_month, 1 );
      $t_start->set_next_month();
      $t_start->set_prev_day();
      $t_now = new exdate();
      if ( $t_start->timestamp_now() > $t_now->timestamp_now() ){
        $t_start = $t_now;
      }

      $t_end   = new exdate( $end_year,$end_month, 1 );

// $this->dump( "{$start_year}/{$start_month}"  );
// $this->dump( "{$end_year}/{$end_month}"  );
// $this->dump( $t_start->now() );
// $this->dump( $t_end->now() );

      //exdb
      $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);
      $count_loop = $count_dt->select(array(
        'data_id' =>$this->q['data_id'] ,
      ),0,999999);
// $this->dump( "全件カウント：". count($count_loop) );


      $chose_loop = array();
      foreach ($count_loop as $k => $v ) {
        $t_tmp = new exdate( $v['access_date'] );
        if ( ( $t_start->timestamp_now() >= $t_tmp->timestamp_now() )&&  ( $t_end->timestamp_now() <= $t_tmp->timestamp_now() ) ){
          array_push($chose_loop, $v);
        }
      }
// $this->dump( "選択 カウント：". count($chose_loop) );

// $this->dump( $chose_loop );
// $count_dt->help();

      $csv = "# {$data_hash['data_id']} : {$data_hash['data_name']} : {$data_hash['title_name']} : {$this->q['start_date']} - {$this->q['end_date']} "."\n";
      while( $t_start->timestamp_now() >= $t_end->timestamp_now() ){
        list($year, $month, $day, $hour, $min, $sec) = $t_start->now();
        $c_day = 0;
        foreach ($chose_loop as $ck => $cv ) {
          if (strpos($cv['access_date'], "{$year}/{$month}/{$day}") !== false) {
            $c_day++;
          }
        }
        $row = '';
        $row = "{$year}/{$month}/{$day}"."\t".$c_day."\n";
        $csv .= $row;
        $t_start->set_prev_day();
      }

/* OLD
      foreach ($chose_loop as $key => $value) {
        $row = '';
        $row = $value['count_id']."\t".$value['data_id']."\t".$value['data_name']."\t".$value['access_date']."\t"."\n";
        $csv .= $row;
      }
OLD */

      header("Content-Type: application/octet-stream");
      header("Content-Disposition: attachment; filename={$data_hash['data_name']}_{$this->q['start_date']}-{$this->q['end_date']}.csv");
      print $csv;

    }




    function do_convert()
    {
      $this->assigns();

      // 1日以上前のテンポラリファイルを削除
      require_once "exfileupload2.php";
  		$file = new exfileupload2($this->_ff_config['tmp_dir']);
  		$file->delete_tmp();

      $this->template->display('admin_convert.html');
    }




    function _check_upload_files( $dropzone_files = array() ){
      if ( isset($dropzone_files) ){
        if ( count($dropzone_files) != 2){
          die('必ず【data.cgi】【data.csv】2ファイルのみアップロードしてください。');
        }
        $flag_cgi = false; $file_cgi ='';
        $flag_csv = false; $file_csv ='';
        foreach ($dropzone_files as $k => $v) {
          list($tmp_name,$jp_name) = preg_split("/\t/",$v);
          if (strcmp('data.cgi',$jp_name)==0){ $flag_cgi = true; $file_cgi =$tmp_name;}
          if (strcmp('data.csv',$jp_name)==0){ $flag_csv = true; $file_csv =$tmp_name;}
        }
        if( $flag_cgi == false || $flag_csv == false ){
          die('【data.cgi】【data.csv】をアップロードしてください。');
        }
  		}
      else{
        die('コンバートするファイルをアップロードしてください。');
      }
      return array($file_cgi, $file_csv);

    }



    function _file_convert_encoding($from_file, $to_file,  $from_enc, $to_enc){

      $data = file_get_contents($from_file);
      $data = mb_convert_encoding($data, $to_enc, $from_enc);

      $filename = $to_file;
      $tmp_filename = getmypid().'.tmp';
      $fp = fopen($tmp_filename, 'w');
      fwrite($fp, $data);
      fclose( $fp );
      rename($tmp_filename, $filename);
    }



    function _preview_file( $filename ,$max_count=10)
    {
      $preview = '';
      $fp = fopen($filename,'r') or die("ファイル({$filename})のオープンに失敗しました");
      $c = 0;
      while ( ($line = fgets($fp,99999)) !== false ) { // 1行あたり最大99999bytes
          $preview .= $line;
          $c++;
          if ($c >= $max_count){ break; }
      }
      fclose($fp);
      return $preview;
    }



    function do_convert_confirm()
    {

      // data.cgi : カウンターファイル（ascii タブ区切り CSV）
      // data.csv : データファイル   （sjis カンマ区切り CSV）
      list($count_file_tsv, $data_file_csv) = $this->_check_upload_files( $this->q['dropzone_files']);
      // $this->dump( "{$this->_ff_config['tmp_dir']}/{$count_file_tsv}" );
      // $this->dump( "{$this->_ff_config['tmp_dir']}/{$data_file_csv}" );

      //utf-8に変換
      $from_file = "{$this->_ff_config['tmp_dir']}/{$count_file_tsv}";
      $to_file   = "{$this->_ff_config['tmp_dir']}/utf8_{$count_file_tsv}";
      $this->_file_convert_encoding($from_file, $to_file, 'sjis-win', 'UTF-8');

      //utf-8に変換
      $from_file = "{$this->_ff_config['tmp_dir']}/{$data_file_csv}";
      $to_file   = "{$this->_ff_config['tmp_dir']}/utf8_{$data_file_csv}";
      $this->_file_convert_encoding($from_file, $to_file, 'sjis-win', 'UTF-8');

      $count_tsv = 0;
      $fp = fopen( "{$this->_ff_config['tmp_dir']}/{$count_file_tsv}", 'r' );
      for( $count_tsv = 0; fgets( $fp ); $count_tsv++ );
      // $this->dump( $count_tsv );

      $preview_tsv = $this->_preview_file("{$this->_ff_config['tmp_dir']}/utf8_{$count_file_tsv}",50);
      $preview_csv = $this->_preview_file("{$this->_ff_config['tmp_dir']}/utf8_{$data_file_csv}",50);



      $count_csv = 0;
      $fp = fopen( "{$this->_ff_config['tmp_dir']}/{$data_file_csv}", 'r' );
      for( $count_csv = 0; fgets( $fp ); $count_csv++ );
      // $this->dump( $count_csv );


      // tsvファイルはファイル順を逆にする
      $reverse_file = $this->_create_reverse_file("{$this->_ff_config['tmp_dir']}/utf8_{$count_file_tsv}");
      // OFF $this->q['utf8_tsv'] = "{$this->_ff_config['tmp_dir']}/utf8_{$count_file_tsv}";
      $this->q['utf8_tsv'] = $reverse_file;



      $this->q['utf8_csv'] = "{$this->_ff_config['tmp_dir']}/utf8_{$data_file_csv}";
      $hidden = $this->make_hidden();
      $this->assign(array('hidden' => $hidden));

      // $this->assign(array('utf8_tsv' => "{$this->_ff_config['tmp_dir']}/utf8_{$count_file_tsv}"));
      // $this->assign(array('utf8_csv' => "{$this->_ff_config['tmp_dir']}/utf8_{$data_file_csv}"));
      $this->assign(array('count_tsv' => $count_tsv));
      $this->assign(array('count_csv' => $count_csv));
      $this->assign(array('preview_tsv' => $preview_tsv));
      $this->assign(array('preview_csv' => $preview_csv));
      $this->template->display('admin_convert_confirm.html');
    }



    function _convert_csv($file_from)
    {
      $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
      $data_dt->delete_all();
// $data_dt->help();
// $this->dump( $data_dt->_config );

      $filename = $file_from;
      $fp = fopen($filename,'r') or die("ファイル({$filename})のオープンに失敗しました");
      $c - 0;
      while ( ($line = fgets($fp,99999)) !== false ) {
          $column = preg_split("/,/",trim($line) );
          for ($i=0; $i < count($column) ; $i++) {
            $column[$i] = preg_replace("/#double#/", '"', $column[$i]);
            $column[$i] = htmlspecialchars_decode($column[$i]);
          }
// $this->dump( $column );
          $insert_hash = array(
            'data_id'       => $column[0] ,
            'data_name'     => $column[1] ,
            'url_name'      => $column[2] ,
            'title_name'    => $column[4] ,
            'analyzer_name' => $column[5] ,
          );
          $data_dt->insert($insert_hash);
          $c++;
      }
      fclose($fp);
      return $c;
    }



    function _create_reverse_file($input_file)
    {
        // reverse
        $reverse_filename = $input_file.'_reverse';
        $reverse_fp = fopen($reverse_filename, 'w');

        $a = file($input_file);
        $a_rev = array_reverse($a);
        foreach ($a_rev as $key => $value) {
          fwrite($reverse_fp, $value);
        }
        fclose($reverse_fp);
        return $reverse_filename;
    }



    function _convert_tsv($file_from)
    {

      // // ファイルを逆順にする
      // $reverse_file = $this->_create_reverse_file($file_from);
      // $this->dump($reverse_file  ); die;

      //         // 1<>1<>a8<>2016/01/26 23:35:37
      $count_dt = new textdb("{$this->rootdir}/textdb.yml", 'count_dt', $this->_ff_config['data_dir']);
      $count_dt->delete_all();
// $count_dt->help();
// $this->dump( $count_dt->_config['count_dt'] ); die;

      $filename = $file_from;
      $fp = fopen($filename,'r') or die("ファイル({$filename})のオープンに失敗しました");
      // データ件数を調査
      $sum = 0;
      while ( ($line = fgets($fp,99999)) !== false ) {
          $sum++;
      }
      fseek($fp, 0);



      $out_filename = $this->_ff_config['data_dir'].'/count_dt.cgi';
      $tmp_filename = $this->_ff_config['data_dir'].'/'.getmypid().'.tmp';
      $out_fp = fopen($tmp_filename, 'w');

      // data_id作成用の変換テーブルをメモリ上に作成
      $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
      $data_loop = $data_dt->select( array() ,0,99999);
      $id_table = array();
      foreach ($data_loop as $dk => $dv) {
        $id_table[$dv['data_name']] = $dv['data_id'];
      }
      // $this->dump( $data_loop );
      // $this->dump( $id_table );

      $c = $sum;
      while ( ($line = fgets($fp,99999)) !== false ) {
        // echo "$c<br>";
          $column = preg_split("/\t/",trim($line) );
          for ($i=0; $i < count($column) ; $i++) {
            $column[$i] = preg_replace("/#double#/", '"', $column[$i]);
            $column[$i] = htmlspecialchars_decode($column[$i]);
          }
// $this->dump( $column );
          preg_match("/([0-9]{4})([0-9]{2})([0-9]{2})/",$column[1], $r);
          // $this->dump( $r );

          // data_idの作成
          $data_id = false;
          $data_name = $column[0];
          $data_id = $id_table[$data_name];
          if (! $data_id){
            die("コンバートエラー : data_id が取得できません。");
          }

          // // data_idの作成（OLD）
          // $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'data_dt', $this->_ff_config['data_dir']);
          // $data_loop = $data_dt->select( array(
          //   'data_name' => $column[0] ,
          // ) ,0,1);

          // 日付けの作成
          require_once './phplib/flatframe/exdate.php';
          $t = new exdate( $r[1], $r[2], $r[3] );
          list($year, $month, $day, $hour, $min, $sec) = $t->now();

          $insert_hash = array(
            'count_id'    => $c ,

          	'data_id'     => $data_loop[0]['data_id'] ,
            'data_id'     => $data_id ,

          	// 'data_name'   => $column[0] ,
            'data_name'   => '' ,

          	'access_date' => "{$year}/{$month}/{$day} {$hour}:{$min}:{$sec}" ,
          );


          $write_data = '';
          foreach ($insert_hash as $kkk => $vvv) {
// $this->dump( $vvv );
            $vvv = $count_dt->_encode_csv($vvv);
            $write_data .= $vvv.'<>';
          }
          $write_data = preg_replace("/<>$/",'',$write_data);
          $write_data = $write_data."\n";
          fwrite($out_fp, $write_data);
          $c--;
      }
      fclose($fp);
      fclose($out_fp);
      rename($tmp_filename, $out_filename);

      // .cout.cgi ファイル
      $count_filename = $this->_ff_config['data_dir'].'/count_dt.count.cgi';
      $count_fp = fopen($count_filename, 'w');
      fwrite($count_fp, "{$sum}\n");
      fclose($count_fp);


      return $sum;
    }


    function do_convert_submit()
    {
// $this->dump( $this->q );

      // データファイル csv を変換
      $count_csv = $this->_convert_csv($this->q['utf8_csv'] );

      // // カウントファイル tsv を変換
      $count_tsv = $this->_convert_tsv($this->q['utf8_tsv'] );

      $this->_write_public_pages();


      $this->assign(array('title' => 'データコンバート完了'));
      $this->assign(array('message' => "<h4>データコンバートが完了しました。</h4> - データファイルを{$count_csv}件<br> - カウントファイルを{$count_tsv}件<br>登録しました。"));
      $_SESSION['title'] = 'データコンバート完了';
      $_SESSION['message'] = "<h4>データコンバートが完了しました。</h4> - データファイルを{$count_csv}件<br> - カウントファイルを{$count_tsv}件<br>登録しました。";
      header("Location: {$this->q['back_url']}?cmd=submit_redirect");
    }


    function do_html_preview()
    {
// $this->dump( $this->q );
        $html = @file_get_contents($this->_ff_config['public_html_dir'].'/'.$this->q['data_name'].'.php');
        $preview = '';
        if ($html){
          $html = preg_replace("/</","&lt;",$html);
          $array = explode("\n", $html); // とりあえず行に分割
          $flag = 0;
          foreach ($array as $key => $value) {
            // $this->dump( $value );
            if ( preg_match("/\?>&lt;html/",$value) ){
              $flag = 1;
              $value = preg_replace("/\?>&lt;html/", "&lt;html", $value);
            }
            if ($flag){
              $preview .= $value."\n";
            }
          }
        }

        if (! $preview){
          print "<h3>ファイル {$this->q['data_name']}.php がありません。<br>「データ編集」または「HTMLテンプレート編集」<br>を行うと リンク変換phpファイルが生成されます。</h3>";
          print "<div>（データの内容は変更せずそのまま【送信】ボタンを押すだけでOKです。）</div>";
        }
        else{
          print "<div style='margin:0 30px 0 30px;padding:0;'>{$this->q['data_name']}.php（html部分のみ表示）</div>";
          print "<pre style='margin:0 30px 30px 30px; border:1px solid #666;padding:10px;word-wrap:break-word;'>\n";
          print $preview;
          print "</pre>\n";
        }

    }







    function do_submit_redirect()
    {
        $this->assigns();
        $this->assign(array('title'   => $_SESSION['title'] ));
        $this->assign(array('message' => $_SESSION['message'] ));
        $this->template->display('admin_submit.html');
    }



    function do_admin_tmpl_edit()
    {
        $data_dt = new textdb("{$this->rootdir}/textdb.yml", 'admin_dt', $this->_ff_config['data_dir']);
        $data_loop = $data_dt->select(array('admin_id' => 1), 0, 1);
        // $this->dump( $data_loop );
        $hash = array();
        $hash = $data_loop[0];
        if (!isset($hash['admin_id'])) {
            $data_dt->insert(array(
              'common_analyzer_name_1' => ''
            ) );
            $data_loop = $data_dt->select(array('admin_id' => 1), 0, 1);
            $hash = $data_loop[0];
        }

        $this->assigns();
        $this->assign($hash);
        // $this->dump( $back_url );
        $hidden = $this->make_hidden();
        $this->assign(array('hidden' => $hidden));
        $this->assign(array('config' => $this->_ff_config));
        $this->assign(array('db_desc' => $data_dt->_config['data_dt']['desc']));
        $html = $this->template->fetch('admin_tmpl_edit.html');
        $fill = new HTML_FillInForm();
        $output = '';
        $output = $fill->fill(array('scalar' => $html, 'fdat' => $hash));
        echo $output;
    }



    function do_admin_tmpl_edit_submit()
    {
        $admin_dt = new textdb("{$this->rootdir}/textdb.yml", 'admin_dt', $this->_ff_config['data_dir']);
        $insert_hash = array();
        foreach ($admin_dt->_config['admin_dt']['desc'] as $k => $v) {
            $column_name = $v[0];
            if (isset($this->q[$column_name])) {
                $insert_hash[$column_name] = $this->q[$column_name];
            }
        }
        // $this->dump( $insert_hash );
        $insert_hash['admin_id'] = 1;
        $result_id = $admin_dt->update($insert_hash);
        $this->_write_public_pages();
        header("Location: {$this->q['back_url']}");
    }



    function _move($from, $to)
    {
        if (copy($from, $to)) {
            unlink($from);

            return true;
        } else {
            die("cannot move file $from  --->   $to");

            return false;
        }
    }



    function delete_allfile($dirpath = '')
    {
        if (strcmp($dirpath, '') == 0) {
            die('delete_allfile : error : please set dir_name');
        }
        $deleted_list = array();
        $dir = dir($dirpath);
        while (($file = $dir->read()) !== false) {
            if (preg_match('/^\./', $file)) {
                continue;
            } else {
                array_push($deleted_list, $file);
                if (!unlink("$dirpath/$file")) {
                    die("delete_allfile : error : can not delete file [{$dirpath}/{$file}]");
                }
            }
        }

        return $deleted_list;
    }
    //========================== make_uri：version 1.4
    function _make_uri($base = '', $rel_path = '')
    {
        $base = preg_replace('/\/[^\/]+$/', '/', $base);
        $parse = parse_url($base);
        if (preg_match('/^https\:\/\//', $rel_path)) {
            return $rel_path;
        } elseif (preg_match('/^\/.+/', $rel_path)) {
            $out = $parse['scheme'].'://'.$parse['host'].$rel_path;

            return $out;
        }
        $tmp = array();
        $a = array();
        $b = array();
        $tmp = preg_split("/\//", $parse['path']);
        foreach ($tmp as $v) {
            if ($v) {
                array_push($a, $v);
            }
        }
        $b = preg_split("/\//", $rel_path);
        foreach ($b as $v) {
            if (strcmp($v, '') == 0) {
                continue;
            } elseif ($v == '.') {
            } elseif ($v == '..') {
                array_pop($a);
            } else {
                array_push($a, $v);
            }
        }
        $path = implode('/', $a);
        $out = $parse['scheme'].'://'.$parse['host'].'/'.$path;

        return $out;
    }
}
