<?php /* Smarty version 2.6.29, created on 2016-01-26 21:20:06
         compiled from inc/html_head.html */ ?>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script src="./js/jquery-1.7.2.min.js"></script>
<script src="./js/bootstrap.js" defer></script>
<script src="./js/notrepeat.js" defer></script>
<script src="./js/jquery_accessing.js" defer></script>
<script src="./js/clipboard.min.js" defer></script>
<link href="./css/bootstrap.min.css" rel="stylesheet">
<link href="./css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
<!-- <link href="./css/font-awesome.css" rel="stylesheet"> -->

<link href="./css/style.css" rel="stylesheet">
<link href="./css/pages/dashboard.css" rel="stylesheet">
<link href="./css/parts.css" rel="stylesheet">
<!-- <link href="./css/font-awesome.css" rel="stylesheet"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js" defer></script>
<![endif]-->