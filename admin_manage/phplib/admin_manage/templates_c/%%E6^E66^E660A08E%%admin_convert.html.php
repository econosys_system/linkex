<?php /* Smarty version 2.6.29, created on 2016-01-29 13:55:45
         compiled from admin_convert.html */ ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>データコンバート</title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/html_head.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<link href="./css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="./js/dropzone.js"></script>
<script src="./js/dropzone_config.js"></script>
<script src="./js/jquery.disableOnSubmit.js"></script>

<?php echo '
<script>
$(function() {
$("#FM").disableOnSubmit();
});
</script>
'; ?>

</head>
<body>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">



<div class="span12">
<div class="widget widget-table action-table">
<div class="widget-header">

<h3>データコンバート</h3>
</div><!-- /widget-header -->

<form name="FM" id="FM" method="post" enctype="multipart/form-data" action="<?php echo $this->_tpl_vars['_program_uri']; ?>
" onsubmit="accessing('accessing');" >
<?php echo $this->_tpl_vars['hidden']; ?>


<input type="hidden" name="cmd" value="convert_confirm">
<div class="widget-content">


<table class="table table-striped table-bordered">
<thead>
</thead>
<tbody>

<tr>
  <td colspan="3" style="text-align:center;">旧ツールの「data.csv」 , 「data.cgi」の2ファイルをアップロードしてください。</td>
</tr>

  <tr>
  <td><b>コンバート元データ</b><br><span style="font-size:x-small;">data.csv , data.cgi</span></td>
  <td>
    <div id="image_drop_area" >ここにアップロードファイルをドロップ<br>（またはクリックするとファイル選択が開きます）</div>

  </td>
  <td class="smaller" style="width:200px;"></td>
  </tr>

  <tr>
  <td><b></b><span style="font-size:x-small;"></span></td>
  <td>
    <div id="preview_area" class="dropzone-custom" style="height:80px;"> </div>
    <div class="clear"></div>
    <div id="preview_reload" style="display:none;"><button class="btn" type="button" onclick="location.href='<?php echo $this->_tpl_vars['_program_uri']; ?>
?cmd=convert'">ファイル選択をやり直す</button></div>
  </td>
  <td class="smaller" style="width:200px;"></td>
  </tr>


  <tr>
<td></td>
<td><input type="submit" id="form_submit" class="btn btn-success">　<button class="btn" onclick="history.back(); return false;">キャンセル</button>
<div class="mt5 small" id="accessing" style="display:none;"><i class="fa fa-refresh fa-2x fa-spin"></i> アクセス中 ...</div>
</td>
</tr>
</tbody>
</table>
</div><!-- .widget-content -->
</form>

</div>
</div>

</div><!-- /row -->
</div><!-- /container -->
</div><!-- /main-inner -->
</div><!-- /main -->


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</body>
</html>
<!-- admin_convert.html -->