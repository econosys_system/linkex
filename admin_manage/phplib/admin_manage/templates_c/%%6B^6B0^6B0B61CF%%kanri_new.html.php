<?php /* Smarty version 2.6.13, created on 2009-02-20 13:56:52
         compiled from kanri_new.html */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<title><?php if ($this->_tpl_vars['cmd'] == 'kanri_edit'): ?>データ修正<?php else: ?>新規データ作成<?php endif; ?></title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/html_head.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<link href="body.css" rel="stylesheet" type="text/css">
<?php echo '
<style type="text/css">
<!--
.style1 {
	font-size: 18px;
	color: #666666;
	font-weight: bold;
}
.style2 {
	color: #333333;
	font-size: 12px;
}
.style3 {font-size: 12px}
.style4 {font-size: 10px}
.style6 {font-size: 10px; color: #666666; }
-->
</style>
'; ?>

</head>
<body>
<table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
 <tr>
  <td valign="top"><table width="700" height="150" border="0" cellpadding="0" cellspacing="0">
    <tr>
     <td><h2><span class="style1"><?php if ($this->_tpl_vars['cmd'] == 'kanri_edit'): ?>データ修正<?php else: ?>新規データ作成<?php endif; ?></span></h2></td>
    </tr>
   </table>
   <table width="700" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td width="200" valign="top">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/navigation.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>  
  
	  </td>
     <td valign="top">
	 
<form name="FM" method="post" enctype="multipart/form-data" action="<?php echo $this->_tpl_vars['_program_name']; ?>
" onSubmit="accessing(); return notrepeat();">
<input type="hidden" name="cmd" value="kanri_new_confirm" >
<?php echo $this->_tpl_vars['hidden']; ?>



       <img src="images/line.gif" width="490" height="5" vspace="5"><br>
       <span class="midashi">新着情報内容を記載の上 「送信する」ボタンをクリックしてください。</span><br>
       <font color="#333333" size="1">※次の確認ページで「確定する」までページは掲載されません。<br>
       ※未入力の内容は表示されません。 </font><br>
       <img src="images/line.gif" width="490" height="5" vspace="5"> <br>
       <table width="490" border="0" cellpadding="0" cellspacing="0">
        <tr>
         <td><table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style2">日付</td>
            <td><select name="year_name" id="year_name">
              <option value="2009">2009</option>
              <option value="2010">2010</option>
              <option value="2011">2011</option>
              <option value="2012">2012</option>
              <option value="2013">2013</option>
              <option value="2014">2014</option>
              <option value="2015">2015</option>
              <option value="2016">2016</option>
              <option value="2017">2017</option>
              <option value="2018">2018</option>
              <option value="2018">2019</option>
             </select>
             <select name="month_name" id="month_name">
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
             </select>
             <select name="day_name" id="day_name">
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
              <option value="26">26</option>
              <option value="27">27</option>
              <option value="28">28</option>
              <option value="29">29</option>
              <option value="30">30</option>
              <option value="31">31</option>
             </select></td>
           </tr>
          </table>
          <img src="images/line.gif" width="490" height="1" vspace="5">
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style2">タイトル</td>
            <td><input name="title_name" type="text" id="title_name" size="40">
            </td>
           </tr>
          </table>
          <img src="images/line.gif" width="490" height="1" vspace="5">
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style2">リンク先</td>
            <td class="style2"><input name="uri_name" type="text" id="uri_name" size="40"><br>
             <label><input name="blank_kbn" type="checkbox" id="blank_kbn" value="1">リンク先を新規ウィンドウで開く</label>
            </td>
           </tr>
          </table>
          <img src="images/line.gif" width="490" height="1" vspace="5">
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td class="style2">

             
             <label><input name="viewtop_kbn" type="checkbox" id="viewtop_kbn" value="1">トップページに表示</label>
             <label><input name="viewarchive_kbn" type="checkbox" id="viewarchive_kbn" value="1">過去の一覧に表示</label>

             </td>
           </tr>
          </table>
          <!--<table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style2">一覧用の説明</td>
            <td><textarea name="setsumei_name" cols="45" rows="２" id="setsumei_name"></textarea></td>
           </tr>
          </table>-->
          </td>
        </tr>
       </table>
       <img src="images/line.gif" width="490" height="5" vspace="5"><br>
       <table width="490" border="0" cellpadding="0" cellspacing="0">
        <tr>
         <td><table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style_blue">見出し小1</td>
            <td><input name="midashi_name1" type="text" id="midashi_name1" size="45"></td>
           </tr>
          </table>
          <img src="images/line.gif" width="490" height="1" vspace="5">
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style_blue">文章1(新着情報)</td>
            <td><textarea name="text_name1" cols="45" rows="7" id="textarea10"></textarea></td>
           </tr>
          </table>
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style_blue">写真1</td>
            <td class="style2">
<?php if ($this->_tpl_vars['photo_file1']): ?>
<img src="../../../data/topics_images/<?php echo $this->_tpl_vars['photo_file1']; ?>
" width="<?php echo $this->_tpl_vars['config']['photo_x']; ?>
">
<!-- <input type="hidden" name="photo_file1_already" value="../../../data/topics_images/<?php echo $this->_tpl_vars['photo_file1']; ?>
">-->
<input type="hidden" name="photo_file1" value="<?php echo $this->_tpl_vars['photo_file1']; ?>
">
<input type="checkbox" name="photo_file1_delete" value="yes">この画像を削除する<br>
<?php endif; ?>
<input name="photo_file1_attach" type="file" id="photo_file1_attach">
			
			</td>
           </tr>
          </table>
          <img src="images/line.gif" width="490" height="5" vspace="5"><br>
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style_green">見出し小2</td>
            <td><input name="midashi_name2" type="text" id="midashi_name2" size="45">
            </td>
           </tr>
          </table>
          <img src="images/line.gif" width="490" height="1" vspace="5">
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style_green">文章2(新着情報)</td>
            <td><textarea name="text_name2" cols="45" rows="7" id="text_name2"></textarea></td>
           </tr>
          </table>
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style_green">写真2</td>
            <td class="style2">
<?php if ($this->_tpl_vars['photo_file2']): ?>
<img src="../../../data/topics_images/<?php echo $this->_tpl_vars['photo_file2']; ?>
" width="<?php echo $this->_tpl_vars['config']['photo_x']; ?>
">
<!-- <input type="hidden" name="photo_file2_already" value="../../../data/topics_images/<?php echo $this->_tpl_vars['photo_file2']; ?>
">-->
<input type="hidden" name="photo_file2" value="<?php echo $this->_tpl_vars['photo_file2']; ?>
">
<input type="checkbox" name="photo_file2_delete" value="yes">この画像を削除する<br>
<?php endif; ?>
<input name="photo_file2_attach" type="file" id="photo_file2_attach">
			</td>

           </tr>
          </table>
          <img src="images/line.gif" width="490" height="5" vspace="5"><br>
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style_orange">見出し小3</td>
            <td><input name="midashi_name3" type="text" id="midashi_name3" size="45">
            </td>
           </tr>
          </table>
          <img src="images/line.gif" width="490" height="1" vspace="5">
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style_orange">文章3(新着情報)</td>
            <td><textarea name="text_name3" cols="45" rows="7" id="text_name3"></textarea></td>
           </tr>
          </table>
          <table width="490" border="0" cellpadding="5" cellspacing="0">
           <tr>
            <td width="100" class="style_orange">写真3</td>
            <td class="style2">
<?php if ($this->_tpl_vars['photo_file3']): ?>
<img src="../../../data/topics_images/<?php echo $this->_tpl_vars['photo_file3']; ?>
" width="<?php echo $this->_tpl_vars['config']['photo_x']; ?>
">
<!-- <input type="hidden" name="photo_file3_already" value="../../../data/topics_images/<?php echo $this->_tpl_vars['photo_file3']; ?>
">-->
<input type="hidden" name="photo_file3" value="<?php echo $this->_tpl_vars['photo_file3']; ?>
">
<input type="checkbox" name="photo_file3_delete" value="yes">この画像を削除する<br>
<?php endif; ?>
<input name="photo_file3_attach" type="file" id="photo_file3_attach">
			</td>
           </tr>
          </table>
          <br></td>
        </tr>
       </table>
       <img src="images/line.gif" width="490" height="5" vspace="5">

       <input type="submit" value="送信する" accesskey="s"">
<p id="accessing" style="display:none;"><img src="common/accessing.gif" /> データ送信中...しばらくお待ち下さい</p>

       <img src="images/line.gif" width="490" height="5" vspace="5">
       <table width="480" border="0" cellspacing="0" cellpadding="0">
        <tr>
         <td><div align="right">
           <input type="reset" name="Submit242" value="内容をクリア">
          </div></td>
        </tr>
       </table>
       <img src="images/line.gif" width="490" height="5" vspace="5">
      </form></td>
    </tr>
   </table></td>
 </tr>
 <tr>
  <td height="20"><span class="style6">&copy;2009 econosys </span></td>
 </tr>
</table>
</body>
</html>
<!-- kanri_new.html -->