<?php /* Smarty version 2.6.29, created on 2016-01-28 08:29:04
         compiled from admin_edit.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'eval', 'admin_edit.html', 73, false),)), $this); ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>データ<?php if ($this->_tpl_vars['cm'] == 'add'): ?>新規作成<?php else: ?>編集<?php endif; ?></title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/html_head.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</head>
<body>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">



<div class="span12">
<div class="widget widget-table action-table">
<div class="widget-header">
<h3>データ<?php if ($this->_tpl_vars['cm'] == 'add'): ?>新規作成<?php else: ?>編集<?php endif; ?></h3>
</div><!-- /widget-header -->

<form name="FM" id="FM_user_dt" method="post" onsubmit="$('#accessing').show(); return notrepeat();" action="<?php echo $this->_tpl_vars['_program_uri']; ?>
">
  <?php echo $this->_tpl_vars['hidden']; ?>

  <?php if ($this->_tpl_vars['cmd'] == 'add'): ?><input type="hidden" name="cmd" value="add_submit">
  <?php elseif ($this->_tpl_vars['cmd'] == 'edit'): ?><input type="hidden" name="cmd" value="edit_submit">
  <?php endif; ?>
  <input type="hidden" name="_back_url" value="">
<div class="widget-content">


<table class="table table-striped table-bordered">
<thead>
</thead>
<tbody>

<?php $_from = $this->_tpl_vars['db_desc']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['loopname'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['loopname']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
        $this->_foreach['loopname']['iteration']++;
?>
<tr>
<td>
<b><?php echo $this->_tpl_vars['v']['options']['view_list_title']; ?>
</b><span style="font-size:x-small;"> ( <?php echo $this->_tpl_vars['v']['0']; ?>
 )</span>
</td>
<td>
  <?php if ($this->_tpl_vars['v']['options']['editable_flg'] == '0'): ?><?php $this->assign('column_name', $this->_tpl_vars['v']['0']); ?><?php if ($this->_tpl_vars['hash']): ?><?php echo smarty_function_eval(array('var' => $this->_tpl_vars['hash'][$this->_tpl_vars['column_name']]), $this);?>
<?php endif; ?>
  <?php else: ?>
    <?php if ($this->_tpl_vars['v']['options']['input_type'] == 'textarea'): ?><textarea name="<?php echo $this->_tpl_vars['v']['0']; ?>
" id="<?php echo $this->_tpl_vars['v']['0']; ?>
" rows="7" style="min-width:400px;"></textarea>
    <?php else: ?><input type="text" name="<?php echo $this->_tpl_vars['v']['0']; ?>
" id="<?php echo $this->_tpl_vars['v']['0']; ?>
" style="min-width:400px;" class="form-control">
    <?php endif; ?>
  <?php endif; ?>
</td>
<td class="smaller" style="width:200px;"><?php echo $this->_tpl_vars['v']['options']['comment']; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?>






      <tr>
<td></td>
<td><button type="submit" class="btn btn-primary">送信</button>　<button class="btn" onclick="history.back(); return false;">キャンセル</button>
  <div class="mt5 small" id="accessing" style="display:none;"><i class="fa fa-refresh fa-spin fa-2x"></i> アクセス中 ...</div>
</td>
<td></td>
</tr>
</tbody>
</table>
</div><!-- .widget-content -->
</form>

</div>
</div>

</div><!-- /row -->
</div><!-- /container -->
</div><!-- /main-inner -->
</div><!-- /main -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


</body>
</html>
<!-- admin_edit.html -->