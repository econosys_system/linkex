<?php /* Smarty version 2.6.29, created on 2016-01-27 19:05:19
         compiled from admin_delete.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'eval', 'admin_delete.html', 46, false),array('modifier', 'escape', 'admin_delete.html', 47, false),array('modifier', 'default', 'admin_delete.html', 47, false),)), $this); ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>データの削除</title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/html_head.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</head>
<body>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">



<div class="span12">
<div class="widget widget-table action-table">
<div class="widget-header">
<h3></h3>
<h3>データの削除</h3>
</div><!-- /widget-header -->

<form name="" method="post" onsubmit="$('#accessing').show(); return notrepeat();" action="<?php echo $this->_tpl_vars['_program_name']; ?>
">
  <?php echo $this->_tpl_vars['hidden']; ?>

  <input type="hidden" name="cmd" value="delete_submit">
<div class="widget-content">


<table class="table table-striped table-bordered">
<thead>
</thead>
<tbody>

  <?php $_from = $this->_tpl_vars['db_desc']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['loopname2'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['loopname2']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['kk'] => $this->_tpl_vars['vv']):
        $this->_foreach['loopname2']['iteration']++;
?>
  <tr>
  <td>
  <b><?php echo $this->_tpl_vars['vv']['options']['view_list_title']; ?>
</b><span style="font-size:x-small;"> ( <?php echo $this->_tpl_vars['vv']['0']; ?>
 )</span>
  </td>
  <td>
    <?php $this->assign('v', $this->_tpl_vars['hash']); ?>
<?php $this->assign('column_name', $this->_tpl_vars['vv']['0']); ?>
<?php if ($this->_tpl_vars['vv']['options']['view_delete_format']): ?><?php echo smarty_function_eval(array('var' => $this->_tpl_vars['vv']['options']['view_delete_format']), $this);?>

<?php else: ?><?php echo smarty_function_eval(array('var' => ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['v'][$this->_tpl_vars['column_name']])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)))) ? $this->_run_mod_handler('default', true, $_tmp, ' ') : smarty_modifier_default($_tmp, ' '))), $this);?>

<?php endif; ?>
  </td>
  <td class="smaller" style="width:200px;"><?php echo $this->_tpl_vars['v']['options']['comment']; ?>
</td>
  </tr>
  <?php endforeach; endif; unset($_from); ?>

<tr>
  <td></td>
  <td>このデータを削除しますか？</td>
  <td></td>
</tr>

<tr>
<td></td>
<td><button type="submit" class="btn btn-danger">削除</button>　<button class="btn" onclick="history.back(); return false;">キャンセル</button>
<div class="mt5 small" id="accessing" style="display:none;"><i class="fa fa-refresh fa-spin fa-2x"></i> アクセス中 ...</div>
</td>
</tr>
</tbody>
</table>
</div><!-- .widget-content -->
</form>

</div>
</div>

</div><!-- /row -->
</div><!-- /container -->
</div><!-- /main-inner -->
</div><!-- /main -->


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</body>
</html>
<!-- excrud_delete.html -->