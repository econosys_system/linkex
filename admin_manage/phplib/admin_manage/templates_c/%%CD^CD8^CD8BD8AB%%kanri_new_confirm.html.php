<?php /* Smarty version 2.6.13, created on 2009-02-18 17:03:32
         compiled from kanri_new_confirm.html */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<title><?php if ($this->_tpl_vars['cmd'] == 'kanri_edit_confirm'): ?>新着情報修正<?php else: ?>新規新着情報作成<?php endif; ?>　確認ページ</title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/html_head.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<link href="body.css" rel="stylesheet" type="text/css">
<?php echo '
<style type="text/css">
<!--
.style1 {
	font-size: 18px;
	color: #666666;
	font-weight: bold;
}
.style2 {
	color: #333333;
	font-size: 12px;
}
.style3 {font-size: 12px}
.style4 {font-size: 10px}
.style6 {font-size: 10px; color: #666666; }
-->
</style>
'; ?>

</head>
<body>
<table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
 <tr>
  <td valign="top"><table width="700" height="150" border="0" cellpadding="0" cellspacing="0">
    <tr>
     <td><h2><span class="style1"><?php if ($this->_tpl_vars['cmd'] == 'kanri_edit_confirm'): ?>新着情報修正<?php else: ?>新規新着情報作成<?php endif; ?>　確認ページ</span></h2></td>
    </tr>
   </table>
   <table width="700" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td width="200" valign="top">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/navigation.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>  
	  </td>
     <td valign="top">

<form name="FM" method="post" action="<?php echo $this->_tpl_vars['_program_name']; ?>
" onsubmit="<?php echo 'accessing(); if (! notrepeat()){return false;}'; ?>
">
<input type="hidden" name="cmd" value="kanri_new_submit" >
<?php echo $this->_tpl_vars['hidden']; ?>

	   
	   <img src="images/line.gif" width="490" height="5" vspace="5"><br>
       <span class="midashi">下記の新着情報を掲載します。<br>
       よろしければ一番下の「内容を確定する」ボタンをクリックしてください。</span><br>
       <img src="images/line.gif" width="490" height="5" vspace="5">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/confirm.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>       
       <img src="images/line.gif" width="490" height="5" vspace="5">
       <input type="button" value="内容を確定する" accesskey="s" onclick="<?php echo 'accessing(); if (notrepeat()){document.FM.submit();}'; ?>
">
<p id="accessing" style="display:none;"><img src="common/accessing.gif" /> データ送信中...しばらくお待ち下さい</p>
       <br>
       <img src="images/line.gif" width="490" height="5" vspace="5">
       <table width="480" border="0" cellspacing="0" cellpadding="0">
        <tr>
         <td><div align="right">
           <input type="button" value="戻る" onclick="history.back();">
          </div></td>
        </tr>
       </table>
       <img src="images/line.gif" width="490" height="5" vspace="5">
      </form></td>
    </tr>
   </table></td>
 </tr>
 <tr>
  <td height="20"><span class="style6"> &copy;2009 econosys </span></td>
 </tr>
</table>
</body>
</html>
<!-- kanri_new_confirm.html -->