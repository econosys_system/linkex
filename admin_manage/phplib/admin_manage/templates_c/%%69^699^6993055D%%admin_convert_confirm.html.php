<?php /* Smarty version 2.6.29, created on 2016-01-27 22:06:30
         compiled from admin_convert_confirm.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin_convert_confirm.html', 53, false),)), $this); ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>データコンバート</title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/html_head.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<link href="./css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="./js/dropzone.js"></script>
<script src="./js/dropzone_config.js"></script>
<script src="./js/jquery.disableOnSubmit.js" defer></script>
<?php echo '
<script>
$(function() {
$("#FM").disableOnSubmit();
});
</script>
'; ?>

</head>
<body>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">



<div class="span12">
<div class="widget widget-table action-table">
<div class="widget-header">
<i class="icon-th-list"></i>
<h3>データコンバート</h3>
</div><!-- /widget-header -->

<form name="FM" id="FM" method="post" enctype="multipart/form-data" action="<?php echo $this->_tpl_vars['_program_uri']; ?>
" onsubmit="$('#accessing').show();" >
<?php echo $this->_tpl_vars['hidden']; ?>


<input type="hidden" name="cmd" value="convert_submit">
<div class="widget-content">


<table class="table table-striped table-bordered">
<thead>
</thead>
<tbody>


  <tr>
  <td><b>元データ（data.csv）：<?php echo $this->_tpl_vars['count_csv']; ?>
件</b><span style="font-size:x-small;"></span></td>
  <td><div><pre style="overflow: scroll; height:100px;"><?php echo ((is_array($_tmp=$this->_tpl_vars['preview_csv'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</pre></div>
<b>（最大50行まで表示します）</b>
  </td>
  </tr>

  <tr>
  <td><b>元データ（data.cgi）：<?php echo $this->_tpl_vars['count_tsv']; ?>
件</b><span style="font-size:x-small;"></span></td>
  <td><div><pre style="overflow: scroll; height:100px;"><?php echo ((is_array($_tmp=$this->_tpl_vars['preview_tsv'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</pre></div>
<b>（最大50行まで表示します）</b>
  </td>
  </tr>

  <tr>
  <td><b></b><span style="font-size:x-small;"></span></td>
  <td>データコンバートを開始します。よろしければコンバート実行ボタンを押してください。<br><b>（コンバートを行うと現在登録中のデータは全て削除されます。）</b></td>
  </tr>


  <tr>
<td></td>
<td><input type="button" value="コンバート実行" onclick="<?php echo '$(\'#accessing\').show();  setTimeout( function(){ document.FM.submit(); },1);'; ?>
" class="btn btn-success">　<button class="btn" onclick="history.back(); return false;">キャンセル</button>
<div class="mt5 small" id="accessing" style="display:none;"><i class="fa fa-refresh fa-2x fa-spin"></i> コンバート中 ... しばらくお待ちください（この処理には時間がかかります）</div>
</td>
</tr>
</tbody>
</table>
</div><!-- .widget-content -->
</form>

</div>
</div>

</div><!-- /row -->
</div><!-- /container -->
</div><!-- /main-inner -->
</div><!-- /main -->


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</body>
</html>
<!-- admin_convert_confirm.html -->