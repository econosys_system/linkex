<?php /* Smarty version 2.6.29, created on 2016-01-28 09:04:06
         compiled from admin_index.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'admin_index.html', 81, false),array('modifier', 'count', 'admin_index.html', 81, false),array('modifier', 'default', 'admin_index.html', 118, false),array('function', 'math', 'admin_index.html', 90, false),array('function', 'eval', 'admin_index.html', 117, false),)), $this); ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>URLリンク管理システム</title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/html_head.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<?php echo '
<script>
$(function () {
var clipboard = new Clipboard(\'.clip_copy_btn\');
});
</script>
'; ?>



<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js" defer></script>
<![endif]-->
<?php echo '
<script>
function dl_check(){
  var check_flag = false;
  $(\'.dl:checked\').each(function(){
    check_flag = true;
  });
  if( check_flag ){
    $(\'#dl_delete\').show();
  }
  else{
    $(\'#dl_delete\').hide();
  }
}
function dl_delete_jump(){
  var arg=\'\';
  $(\'.dl:checked\').each(function(){
    //alert( $(this).val() );
    arg += \'&dl[]=\'+$(this).val();
  });
//  alert(arg);
  if (arg){
    if ( confirm(\'選択したデータを削除します。よろしいですか？\') ){
      location.href = \'http://successplus.jp/instagram_post/excrud_admin/dl_delete_submit/user_dt/?_back_url=\'+encodeURIComponent(location.href)+arg;
    }
  }
}
</script>
<style>
table td {
    word-break: break-all;
}
.my_width{
/*  width:1200px !important; */
}
</style>
'; ?>


</head>
<body>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="main">
<div class="main-inner">
<div class="container my_width">


<div class="row">
<div class="span12" style="text-align: center;">
<div class="pagination mt5 mb5">

</div>
</div>
</div><!-- /row -->

<div class="row">
<div class="span12 my_width">
<div class="widget widget-table action-table">
<div class="widget-header">
<h3>データ：<?php if ($this->_tpl_vars['q']): ?>キーワード「<?php echo ((is_array($_tmp=$this->_tpl_vars['q'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
での検索結果」：<?php endif; ?>合計 <?php echo count($this->_tpl_vars['data_loop']); ?>
 件</h3>
</div><!-- /widget-header -->
<div class="widget-content">

<div class="table-responsive"> <!-- style="overflow-x: scroll; " -->

<table style="height:100%;" class="table table-striped table-bordered table-condensed">

<tr>
<?php echo smarty_function_math(array('equation' => "a+b",'a' => count($this->_tpl_vars['db_desc']),'b' => 2,'assign' => 'kekka'), $this);?>

<td colspan="<?php echo $this->_tpl_vars['kekka']; ?>
">
<span style="display:none;" id="dl_delete"><a href="javascript:dl_delete_jump();;" class="btn btn-large btn-danger btn-support-ask ml5 mt5 mb5"><i class="fa fa-trash fa-lg"></i> 削除</a></span>
<a href="<?php echo $this->_tpl_vars['_program_uri']; ?>
?cmd=add&table_name=data_dt" class="btn btn-large btn-success btn-support-ask ml5 mt5 mb5">データ新規作成</a>
</td>
</tr>
<thead>
<tr>

<?php $_from = $this->_tpl_vars['db_desc']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['dbloopname'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['dbloopname']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['dbk'] => $this->_tpl_vars['dbv']):
        $this->_foreach['dbloopname']['iteration']++;
?>
<th><?php echo $this->_tpl_vars['dbv']['options']['view_list_title']; ?>
</th>
<?php endforeach; endif; unset($_from); ?>
<th>編集</th><th>カウンター</th>
</tr>
</thead>
<tbody>

  <?php $_from = $this->_tpl_vars['data_loop']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['loopname'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['loopname']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
        $this->_foreach['loopname']['iteration']++;
?>
  <tr>
    <!-- <td class="no_padding">
      <label>
        <input type="checkbox" class="dl" onclick="dl_check();" name="data_list[]" value="1">
      </label>
    </td> -->
    <?php $_from = $this->_tpl_vars['db_desc']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['loopname2'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['loopname2']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['kk'] => $this->_tpl_vars['vv']):
        $this->_foreach['loopname2']['iteration']++;
?>
    <?php $this->assign('column_name', $this->_tpl_vars['vv']['0']); ?>
    <td<?php if ($this->_tpl_vars['vv']['options']['view_list_nowrap_flag'] == '1'): ?> nowrap<?php endif; ?>>
    <?php if ($this->_tpl_vars['vv']['options']['view_list_format']): ?><?php echo smarty_function_eval(array('var' => $this->_tpl_vars['vv']['options']['view_list_format']), $this);?>

    <?php else: ?><?php echo smarty_function_eval(array('var' => ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['v'][$this->_tpl_vars['column_name']])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)))) ? $this->_run_mod_handler('default', true, $_tmp, ' ') : smarty_modifier_default($_tmp, ' '))), $this);?>

    <?php endif; ?>
    </td>
    <?php endforeach; endif; unset($_from); ?>
    <td nowrap>
      <a href="<?php echo $this->_tpl_vars['_program_uri']; ?>
?cmd=edit&data_id=<?php echo $this->_tpl_vars['v']['data_id']; ?>
" class="btn btn-small btn-success mb3"><i class="fa fa-pencil"></i> 編集</a><br>
      <a href="<?php echo $this->_tpl_vars['_program_uri']; ?>
?cmd=delete_confirm&data_id=<?php echo $this->_tpl_vars['v']['data_id']; ?>
" class="btn btn-danger btn-small"><i class="fa fa-trash"></i> 削除</a>
     </td>
    <td nowrap> <a href="<?php echo $this->_tpl_vars['_program_uri']; ?>
?cmd=csv_download&data_id=<?php echo $this->_tpl_vars['v']['data_id']; ?>
" class="btn btn-small btn-success mb3">CSVダウンロード</a><br>
      <a href="<?php echo $this->_tpl_vars['_program_uri']; ?>
?cmd=count_reset_confirm&data_id=<?php echo $this->_tpl_vars['v']['data_id']; ?>
" class="btn btn-small btn-success">カウントリセット</a> </td>
  </tr>
  <?php endforeach; endif; unset($_from); ?>

</tbody>
</table>

</div>

</div><!-- .widget-content -->
</div>
</div>
</div><!-- /row -->


<div class="row">
<div class="span12" style="  text-align: center;">
<div class="pagination">

</div>
</div>
</div><!-- /row -->


</div><!-- /row -->
</div><!-- /container -->
</div><!-- /main-inner -->
</div><!-- /main -->



<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</body>
</html>
<!-- excrud_index.html -->