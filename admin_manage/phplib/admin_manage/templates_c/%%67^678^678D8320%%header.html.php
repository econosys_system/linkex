<?php /* Smarty version 2.6.29, created on 2016-01-28 09:54:06
         compiled from inc/header.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'inc/header.html', 19, false),)), $this); ?>
<!-- header.html -->
<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
<div class="container"> <a class="brand" href="<?php echo $this->_tpl_vars['_program_name']; ?>
">URLリンク管理</a>
<div class="nav-collapse">


<form action="<?php echo $this->_tpl_vars['_program_uri']; ?>
" class="navbar-search pull-right">
<input type="hidden" name="cmd" value="search">
<input type="text" name="q" id="q" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['q'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" class="search-query" >
</form>
</div>
<!--/.nav-collapse -->
</div>
<!-- /container -->
</div>
<!-- /navbar-inner -->
</div>
<!-- /navbar -->
<div class="subnavbar">
<div class="subnavbar-inner">
<div class="container">
<ul class="mainnav">
<li><a href="<?php echo $this->_tpl_vars['_program_uri']; ?>
"><i class="fa fa-th-list"></i><span>データ一覧</span> </a> </li>
<li><a href="<?php echo $this->_tpl_vars['_progran_uri']; ?>
?cmd=admin_tmpl_edit&back_url=<?php echo ((is_array($_tmp=$this->_tpl_vars['back_url'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
"><i class="fa fa-code"></i><span>HTMLテンプレート編集</span> </a> </li>
<li><a href="<?php echo $this->_tpl_vars['_progran_uri']; ?>
?cmd=convert"><i class="fa fa-database"></i><span>旧ツールデータ読み込み</span> </a> </li>
<li></li>
</ul>
</div>
<!-- /container -->
</div>
<!-- /subnavbar-inner -->
</div>
<!-- /subnavbar -->
<!-- header.html -->