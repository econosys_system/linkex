<?php /* Smarty version 2.6.29, created on 2016-01-29 18:23:07
         compiled from admin_tmpl_edit.html */ ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>HTMLテンプレート編集</title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/html_head.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


</head>
<body>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">



<div class="span12">
<div class="widget widget-table action-table">
<div class="widget-header">
<h3>HTMLテンプレート編集</h3>
</div><!-- /widget-header -->

<form name="FM" id="FM_user_dt" method="post" onsubmit="$('#accessing').show(); return notrepeat();" action="<?php echo $this->_tpl_vars['_program_uri']; ?>
">
	<input type="hidden" name="cmd" value="admin_tmpl_edit_submit">
  <input type="hidden" name="back_url" value="<?php echo $this->_tpl_vars['back_url']; ?>
">
<div class="widget-content">


<table class="table table-striped table-bordered">
<thead>
</thead>
<tbody>

<tr>
  <td colspan="3" style="text-align:center;">&lt;head&gt;タグ</td>
</tr>

<tr>
<td>
<b>共通アクセス解析 1</b>
</td>
<td>
	<textarea name="common_analyzer_name_1" rows="5" style="min-width:300px; width:500px;"></textarea>
</td>
<td class="smaller" style="width:200px;">ここに記述したHTMLコードは<br>&lt;head&gt; 〜 &lt;/head&gt;タグの間に書きだされます</td>
</tr>

<tr>
  <td colspan="3" style="text-align:center;">&lt;/head&gt;タグ</td>
</tr>
<tr>
  <td colspan="3" style="text-align:center;">&lt;body&gt;タグ</td>
</tr>

<tr>
<td>
<b>共通アクセス解析 2</b>
</td>
<td>
	<textarea name="common_analyzer_name_2" rows="5" style="min-width:300px; width:500px;"></textarea>
</td>
<td class="smaller" style="width:200px;">ここに記述したHTMLコードは<br>&lt;body&gt;タグ直後に書きだされます</td>
</tr>

<tr>
  <td colspan="3" style="text-align:center;">ここにページごとのアクセス解析タグが入ります。</td>
</tr>

<tr>
<td>
<b>共通アクセス解析 3</b>
</td>
<td>
	<textarea name="common_analyzer_name_3" rows="5" style="min-width:300px; width:500px;"></textarea>
</td>
<td class="smaller" style="width:200px;">ここに記述したHTMLコードは<br>&lt;/body&gt;閉じタグ直前に書きだされます</td>
</tr>

<tr>
  <td colspan="3" style="text-align:center;">&lt;/body&gt;タグ</td>
</tr>






      <tr>
<td></td>
<td><button type="submit" class="btn btn-primary">送信</button>　<button class="btn" onclick="history.back(); return false;">キャンセル</button>
  <div class="mt5 small" id="accessing" style="display:none;"><i class="fa fa-refresh fa-spin fa-2x"></i> アクセス中 ...</div>
</td>
<td></td>
</tr>
</tbody>
</table>
</div><!-- .widget-content -->
</form>

</div>
</div>

</div><!-- /row -->
</div><!-- /container -->
</div><!-- /main-inner -->
</div><!-- /main -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


</body>
</html>
<!-- admin_tmpl_edit.html -->