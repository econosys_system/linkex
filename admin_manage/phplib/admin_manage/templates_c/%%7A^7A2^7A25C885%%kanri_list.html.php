<?php /* Smarty version 2.6.29, created on 2016-01-18 14:47:05
         compiled from kanri_list.html */ ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>URLリンク管理システムテーブル『user_dt』</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<script src="http://successplus.jp/instagram_post/excrud/js/jquery-1.7.2.min.js" defer></script>
<script src="http://successplus.jp/instagram_post/excrud/js/bootstrap.js" defer></script>
<script src="http://successplus.jp/instagram_post/excrud/js/jquery.field.min.js" defer></script>
<link href="http://successplus.jp/instagram_post/excrud/css/bootstrap.min.css" rel="stylesheet">
<link href="http://successplus.jp/instagram_post/excrud/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
<link href="http://successplus.jp/instagram_post/excrud/css/font-awesome.css" rel="stylesheet">
<link href="http://successplus.jp/instagram_post/excrud/css/style.css" rel="stylesheet">
<link href="http://successplus.jp/instagram_post/excrud/css/pages/dashboard.css" rel="stylesheet">
<link href="http://successplus.jp/instagram_post/excrud/css/parts.css" rel="stylesheet">
<link href="http://successplus.jp/instagram_post/excrud/css/excrud.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js" defer></script>
<![endif]-->
<?php echo '
<script>
function dl_check(){
  var check_flag = false;
  $(\'.dl:checked\').each(function(){
    check_flag = true;
  });
  if( check_flag ){
    $(\'#dl_delete\').show();
  }
  else{
    $(\'#dl_delete\').hide();
  }
}
function dl_delete_jump(){
  var arg=\'\';
  $(\'.dl:checked\').each(function(){
    //alert( $(this).val() );
    arg += \'&dl[]=\'+$(this).val();
  });
//  alert(arg);
  if (arg){
    if ( confirm(\'選択したデータを削除します。よろしいですか？\') ){
      location.href = \'http://successplus.jp/instagram_post/excrud_admin/dl_delete_submit/user_dt/?_back_url=\'+encodeURIComponent(location.href)+arg;
    }
  }
}
</script>
<style>
table td {
    word-break: break-all;
}
.my_width{
/*  width:1200px !important; */
}
</style>
'; ?>


</head>
<body>

<!-- excrud_header.html -->
<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
  <a class="brand" href="http://successplus.jp/instagram_post/excrud_admin/">
    INSTAGRAM 自動投稿 テーブル『user_dt』 </a>
<div class="nav-collapse">

<ul class="nav pull-right">
  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i> Config <b class="caret"></b></a>

  <ul class="dropdown-menu">
  <li><a href="<?php echo 'javascript:if(confirm(\'jsonファイルを再生成します。よろしいですか？\')){location.href=\'http://successplus.jp/instagram_post/excrud_admin/json_recreate/?back_url=\'+encodeURIComponent(location.href);}'; ?>
">Json Re-Create</a></li>
  </ul>
  </li>

  <!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> Admin <b class="caret"></b></a>
  <ul class="dropdown-menu">
  <li><a href="/am_rms/am_drive/drive_am_category" target="_blank">カテゴリ巡回</a></li>
  <li><a href="/am_rms/am_drive/drive_am_category/?dump=1" target="_blank">カテゴリ巡回（dump）</a></li>
  </ul>
  </li> -->

</ul>

<form action="" class="navbar-search pull-right" onsubmit="location.href=('http://successplus.jp/instagram_post/excrud_admin/search/user_dt/'+ encodeURIComponent($('#q').val()) ); return false;">
<input type="text" name="q" id="q" value="" class="search-query" placeholder="Search">
</form>
</div><!--/.nav-collapse -->
</div><!-- /container -->
</div><!-- /navbar-inner -->
</div><!-- /navbar -->

<div class="subnavbar">
<div class="subnavbar-inner">
<div class="container">
<ul class="mainnav">

  <!-- excrud_header_extend.html -->
  <li><a href="#"><i class="icon-th-list"></i><span>データ一覧</span> </a> </li>
  <li><a href="#"><i class="icon-th-list"></i><span>HTMLテンプレート編集</span> </a> </li>
  <li><a href="#"><i class="icon-th-list"></i><span>旧ツールデータ読み込み</span> </a> </li>

  <li></li>
<!-- /excrud_header_extend.html -->


<!--
  <li class="dropdown">
    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-th-list"></i><span>Tables</span></a>
      <ul class="dropdown-menu">
                <li><a href="http://successplus.jp/instagram_post/excrud_admin/index/user_dt">・user_dt</a></li>
              </ul>
  </li>

  <li class="dropdown">
    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-th-list"></i><span>SQL実行</span></a>
      <ul class="dropdown-menu">
                <li><a href="http://successplus.jp/instagram_post/excrud_admin/sql/user_dt">・user_dt</a></li>
              </ul>
  </li>
-->







    <li></li>




</ul>
</div><!-- /container -->
</div><!-- /subnavbar-inner -->
</div><!-- /subnavbar -->
<!-- /excrud_header.html -->

<div class="main">
<div class="main-inner">
<div class="container my_width">


<div class="row">
<div class="span12" style="text-align: center;">
<div class="pagination mt5 mb5">

</div>
</div>
</div><!-- /row -->

<div class="row">
<div class="span12 my_width">
<div class="widget widget-table action-table">
<div class="widget-header">
<i class="icon-th-list"></i>
<h3>テーブル【user_dt】：合計 1 件</h3>
</div><!-- /widget-header -->
<div class="widget-content">

<div class="table-responsive"> <!-- style="overflow-x: scroll; " -->

<table style="height:100%;" class="table table-striped table-bordered table-condensed">

<tr>
<td colspan=11>
<span style="display:none;" id="dl_delete"><a href="javascript:dl_delete_jump();;" class="btn btn-large btn-danger btn-support-ask ml5 mt5 mb5"><i class="icon-trash icon-lg"></i> 削除</a></span>
<a href="javascript:location.href='http://successplus.jp/instagram_post/excrud_admin/add/user_dt';" class="btn btn-large btn-success btn-support-ask ml5 mt5 mb5">データ新規作成</a>
</td>
</tr>
<thead>
<tr>
<th><i class="icon-check icon-2x"></i></th>
<th>ID</th>
<th>ファイル名</th>
<th>転送URL取得</th>
<th>ジャンルID</th>
<th>タイトル</th>
<th>転送先</th>
<th>当日</th>
<th>当月</th>
<th style="width:58px;">編集</th><th>カウンター</th>
</tr>
</thead>
<tbody>
<tr>
<td class="no_padding">
<label><input type="checkbox" class="dl" onclick="dl_check();" name="data_list[]" value="1"></label>
</td>
<td nowrap>
1
</td>
<td>
a8
</td>
<td>
webpass1
</td>
<td>1</td>
<td>
A8.net　会員募集
</td>
<td>http://px.a8.net/svt/ejp?a8mat=TY4ZK+8MSAK2+0K+ZSD6A

</td>
<td>0

</td>
<td>5</td>

<td>
<a href="<?php echo $this->_tpl_vars['_program_uri']; ?>
?cmd=edit" class="btn btn-small btn-success mb3"><i class="btn-icon-only icon-ok"> </i>編集</a>
<a href="javascript:location.href='http://successplus.jp/instagram_post/excrud_admin/delete/user_dt/1?_back_url='+encodeURIComponent(location.href);" class="btn btn-danger btn-small"><i class="btn-icon-only icon-remove"> </i>削除</a>
</td>

<td>
<a href="#" class="btn btn-small btn-success mb3"><i class="fa-download icon-ok"> </i>CSVダウンロード</a>
<a href="#" class="btn btn-small btn-success mb3"><i class="fa-circle-o-notch icon-ok"> </i>カウントリセット</a>
</td>

</tr>
</tbody>
</table>

</div>

</div><!-- .widget-content -->
</div>
</div>
</div><!-- /row -->


<div class="row">
<div class="span12" style="  text-align: center;">
<div class="pagination">

</div>
</div>
</div><!-- /row -->


</div><!-- /row -->
</div><!-- /container -->
</div><!-- /main-inner -->
</div><!-- /main -->



<!-- excrud_footer.html -->
<div class="footer">
<div class="footer-inner">
<div class="container">
<div class="row">
<div class="span12">
&copy; 2016 <a href="http://flatsystems.net/econosys_system/" target="_blank">econosys system</a>
</div><!-- /span12 -->
</div><!-- /row -->
</div><!-- /container -->
</div><!-- /footer-inner -->
</div><!-- /footer -->
<!-- /excrud_footer.html -->


</body>
</html>
<!-- excrud_index.html -->