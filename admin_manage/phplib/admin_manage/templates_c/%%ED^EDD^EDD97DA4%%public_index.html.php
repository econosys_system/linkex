<?php /* Smarty version 2.6.13, created on 2009-02-24 17:08:43
         compiled from public_index.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'public_index.html', 119, false),)), $this); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta name="keywords" content="宮脇賣扇庵,京扇子,扇子,京都">
<meta name="Description" content="宮脇賣扇庵の公式ウェブサイトです。">
<title>宮脇賣扇庵｜京扇子</title>
<script type="text/javascript" src="common/rollover.js"></script>
<script type="text/javascript" src="common/open.js"></script>
<script type="text/javascript" src="common/scroll.js"></script>
<link href="common/body.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="MM_preloadImages('menu/menu2_01.gif','menu/menu2_02.gif','menu/menu2_03.gif','menu/menu2_04.gif','menu/menu2_05.gif','menu/menu2_06.gif','menu/menu2_07.gif','menu/menu2_08.gif','menu/menu2_09.gif','menu/top_btn2_01.gif','images/news_btn2.gif')">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
 <tr>
  <td><table width="830" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td><a href="index.html"><img src="common/logo.gif" alt="京扇子｜宮脇賣扇庵" width="97" height="30" hspace="38" vspace="18" border="0"></a></td>
     <td valign="top" class="text_02"><div align="right">
       <h1 class="head_text">京扇子｜宮脇賣扇庵</h1>
      </div></td>
    </tr>
   </table>
   <a href="index.html"></a></td>
 </tr>
 <tr>
  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
     <td width="15"><img src="common/clear.gif" width="15" height="1"></td>
     <td bgcolor="#1F2D04"><table width="100%" border="0" cellpadding="0" cellspacing="0">
       <tr>
        <td height="4" background="common/back_top_02.gif" bgcolor="#FFFFFF"><img src="common/back_top_01.gif" width="8" height="4"></td>
       </tr>
       <tr>
        <td><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
           <td width="4" valign="top" background="common/back_left_02.gif" bgcolor="#FFFFFF"><img src="common/back_left_01.gif" width="4" height="4"></td>
           <td valign="top"><table border="0" cellspacing="0" cellpadding="0">
             <tr>
              <td><img src="common/clear.gif" width="1" height="1"></td>
             </tr>
             <tr>
              <td><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                 <td><img src="common/clear.gif" width="19" height="1"></td>
                 <td><a href="index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image16','','menu/menu2_01.gif',1)"><img src="menu/menu2_01.gif" alt="トップページ" name="Image16" width="97" height="36" border="0"></a></td>
                 <td><img src="common/clear.gif" width="1" height="1"></td>
                 <td><a href="about_01.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','menu/menu2_02.gif',1)"><img src="menu/menu_02.gif" alt="宮脇賣扇庵について" name="Image18" width="136" height="36" border="0"></a></td>
                 <td><img src="common/clear.gif" width="1" height="1"></td>
                 <td><a href="shop_01.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image19','','menu/menu2_03.gif',1)"><img src="menu/menu_03.gif" alt="店舗のご案内" name="Image19" width="102" height="36" border="0"></a></td>
                 <td><img src="common/clear.gif" width="1" height="1"></td>
                 <td><a href="sensu_01.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image20','','menu/menu2_04.gif',1)"><img src="menu/menu_04.gif" alt="宮脇賣扇庵の扇子" name="Image20" width="126" height="36" border="0"></a></td>
                 <td><img src="common/clear.gif" width="1" height="1"></td>
                 <td><a href="culture_01.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image21','','menu/menu2_05.gif',1)"><img src="menu/menu_05.gif" alt="扇子の伝統と文化" name="Image21" width="125" height="36" border="0"></a></td>
                 <td><img src="common/clear.gif" width="1" height="1"></td>
                 <td><a href="company.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image22','','menu/menu2_06.gif',1)"><img src="menu/menu_06.gif" alt="会社のご案内" name="Image22" width="102" height="36" border="0"></a></td>
                 <td><img src="common/clear.gif" width="1" height="1"></td>
                 <td><a href="contact_01.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image23','','menu/menu2_07.gif',1)"><img src="menu/menu_07.gif" alt="お問い合わせ" name="Image23" width="106" height="36" border="0"></a></td>
                </tr>
               </table></td>
             </tr>
             <tr>
              <td><img src="common/clear.gif" width="1" height="4"></td>
             </tr>
             <tr>
              <td><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                 <td><img src="common/clear.gif" width="14" height="1"></td>
                 <td><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="810" height="410">
                   <param name="movie" value="top_flash.swf">
                   <param name="quality" value="high">
                   <embed src="top_flash.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="810" height="410"></embed>
                  </object></td>
                </tr>
               </table></td>
             </tr>
             <tr>
              <td><img src="common/clear.gif" width="1" height="25"></td>
             </tr>
             <tr>
              <td><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                 <td><img src="common/clear.gif" width="45" height="1"></td>
                 <td width="773"><table width="773" border="0" cellpadding="0" cellspacing="0">
                   <tr>
                    <td><table width="773" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                       <td valign="top"><table border="0" cellspacing="0" cellpadding="0">
                         <tr>
                          <td class="midashi_b01"><img src="midashi/top_midash.gif" alt="気品と優美に彩られ、いまに受け継ぐ伝統美。" width="352" height="32"></td>
                         </tr>
                         <tr>
                          <td class="text_01"><img src="images/top_image_04.jpg" alt="宮脇賣扇庵" width="165" height="165" align="left" style="margin:0px 5px 0px 0px">創業文政6年。京都市街の中心部、東西に走る六角通りと南北に走る富小路通りの交差点を東へ少し入ったところ。宮脇賣扇庵は、虫籠窓に紅殻格子、表に張り出した床几など、古き良き京の面影をとどめる近世の町家そのままの店構えで、観光客の観光ルートとしても知られています。しかし、店構えだけでなく、舞扇や夏扇に加え、昔ながらの桧扇や飾扇など、あらゆる扇をそろえる老舗というのがその素顔です。店内には、明治期の京都画壇の巨匠四十八画伯によって描かれた天井画などの絵画や書などが多く残され、京都や日本を代表する画家との深いつながりを今に伝えています。</td>
                         </tr>
                        </table></td>
                       <td valign="top"><img src="common/clear.gif" width="30" height="1"></td>
                       <td width="260" valign="top"><table width="250" cellspacing="0" cellpadding="0">
                         <tr>
                          <td class="news_layout"><table width="250" cellspacing="0" cellpadding="0">
                            <tr>
                             <td><img src="images/news_01.gif" alt="新着情報" width="139" height="32"></td>
                             <td><a href="news_the_past.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image36','','images/news_btn2.gif',1)"><img src="images/news_btn.gif" alt="過去の一覧" name="Image36" width="81" height="32" border="0"></a></td>
                            </tr>
                           </table></td>
                         </tr>
                         <tr>
                          <td bgcolor="#001122"><img src="common/clear.gif" width="1" height="1"></td>
                         </tr>
                         <tr>
                          <td bgcolor="#444444"><img src="common/clear.gif" width="1" height="1"></td>
                         </tr>

<?php $_from = $this->_tpl_vars['data_loop']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['loopname'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['loopname']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
        $this->_foreach['loopname']['iteration']++;
 if ($this->_tpl_vars['v']['viewtop_kbn'] == 1): ?>
                         <tr bgcolor="#303920">
                          <td class="text_news"><strong><?php echo $this->_tpl_vars['v']['year_name']; ?>
/<?php echo $this->_tpl_vars['v']['month_name']; ?>
/<?php echo $this->_tpl_vars['v']['day_name']; ?>
</strong><br>
                           <?php if ($this->_tpl_vars['v']['midashi_name1'] || $this->_tpl_vars['v']['text_name1'] || $this->_tpl_vars['v']['midashi_name2'] || $this->_tpl_vars['v']['text_name2'] || $this->_tpl_vars['v']['midashi_name3'] || $this->_tpl_vars['v']['text_name3']): ?><a href="javascript:subWin('./data_topics/<?php echo $this->_tpl_vars['v']['data_id']; ?>
.html','contact','width=620,height=620,toolbar=no,scrollbars=yes')" class="text_link"><?php echo ((is_array($_tmp=$this->_tpl_vars['v']['title_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a>
                            <?php elseif ($this->_tpl_vars['v']['uri_name']): ?><a href="<?php echo $this->_tpl_vars['v']['uri_name']; ?>
" <?php if ($this->_tpl_vars['v']['blank_kbn'] == 1): ?>target="_blank" <?php endif; ?> class="text_link"><?php echo ((is_array($_tmp=$this->_tpl_vars['v']['title_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</a>
                            <?php else:  echo ((is_array($_tmp=$this->_tpl_vars['v']['title_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>

<?php endif; ?>

                       </td>
                         </tr>
                         <tr>
                          <td bgcolor="#001122"><img src="common/clear.gif" width="1" height="1"></td>
                         </tr>
                         <tr>
                          <td bgcolor="#444444"><img src="common/clear.gif" width="1" height="1"></td>
                         </tr>
<?php endif;  endforeach; endif; unset($_from); ?>
                         <tr>
                          <td><img src="common/clear.gif" width="1" height="20"></td>
                         </tr>
                         <tr>
                          <td><a href="http://shop.ikyu.com/shMain/shShop.asp?shopId=500321" target="_blank"><img src="images/banner.gif" width="250" height="80" border="0"></a></td>
                         </tr>
                        </table></td>
                      </tr>
                     </table></td>
                   </tr>
                   <tr>
                    <td><img src="common/clear.gif" width="1" height="30"></td>
                   </tr>
                  </table></td>
                </tr>
               </table></td>
             </tr>
            </table></td>
          </tr>
         </table></td>
       </tr>
       <tr>
        <td height="4" background="common/back_bottom_02.gif" bgcolor="#FFFFFF"><img src="common/back_bottom_01.gif" width="8" height="4"></td>
       </tr>
       <tr>
        <td bgcolor="#FFFFFF"><table border="0" cellspacing="0" cellpadding="0">
          <tr>
           <td width="24"><img src="common/clear.gif" width="24" height="1"></td>
           <td width="70"><a href="sitemap.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image25','','menu/menu2_08.gif',1)"><img src="menu/menu_08.gif" alt="サイトマップ" name="Image25" width="70" height="28" border="0"></a></td>
           <td width="15"><img src="common/clear.gif" width="15" height="1"></td>
           <td width="69"><a href="rules.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image27','','menu/menu2_09.gif',1)"><img src="menu/menu_09.gif" alt="ご利用規約" name="Image27" width="69" height="28" border="0"></a></td>
           <td width="11"><img src="common/clear.gif" width="434" height="1"></td>
           <td width="11"><img src="common/copyright.gif" alt="Miyawaki baisen Co.,Ltd. All Rights Reserved." width="211" height="28"></td>
          </tr>
         </table></td>
       </tr>
       <tr>
        <td bgcolor="#FFFFFF">&nbsp;</td>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
</body>
</html>