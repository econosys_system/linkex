<?php /* Smarty version 2.6.29, created on 2016-01-27 18:14:02
         compiled from admin_error.html */ ?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title><?php echo $this->_tpl_vars['title']; ?>
</title>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/html_head.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<link href="./css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="./js/dropzone.js"></script>
<script src="./js/dropzone_config.js"></script>
</head>
<body>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="main">
<div class="main-inner">
<div class="container">
<div class="row">



<div class="span12">
<div class="widget widget-table action-table">
<div class="widget-header">
<h3><?php echo $this->_tpl_vars['title']; ?>
</h3>
</div><!-- /widget-header -->

<form name="FM" method="post" enctype="multipart/form-data" action="<?php echo $this->_tpl_vars['_program_uri']; ?>
" onsubmit="accessing('accessing');" >
<?php echo $this->_tpl_vars['hidden']; ?>


<input type="hidden" name="cmd" value="convert_submit">
<div class="widget-content">


<table class="table table-striped table-bordered">
<thead>
</thead>
<tbody>



  <tr>
  <td><b></b><span style="font-size:x-small;"></span></td>
  <td><?php echo $this->_tpl_vars['message']; ?>
</td>
  <td class="smaller" style="width:200px;"></td>
  </tr>


<tr>
<td></td>
<td><button type="button" class="btn btn-success" onclick="history.back();">戻る</button>
<div class="mt5 small" id="accessing" style="display:none;"><i class="fa fa-refresh fa-2x fa-spin"></i> アクセス中 ...</div>
</td>
</tr>

</tbody>
</table>
</div><!-- .widget-content -->
</form>

</div>
</div>

</div><!-- /row -->
</div><!-- /container -->
</div><!-- /main-inner -->
</div><!-- /main -->


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "inc/footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</body>
</html>
<!-- admin_error.html -->