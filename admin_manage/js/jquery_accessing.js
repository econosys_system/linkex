// jquery_accessing.js (c)2002-2011 econosys system
// http://www.econosys.jp/system/
//
// 要：jquery.js
//

// Version
//	1.0  とりあえず作成
//	1.1  j$をやめた

var accessing_id_name = 'accessing';		// デフォルトのid名前


//----------------------------------------------- event-unload

// Event.observe(window, "unload", hide_accessing, false);
jQuery(window).unload(function(){
    hide_accessing();
});

//----------------------------------------------- accessing
function accessing(id_name){

	if (! id_name ){}
	else{ accessing_id_name=id_name; }

	if (! jQuery('#'+accessing_id_name) ){ alert('accessing: not found id ['+accessing_id_name+']'); return false;}

	usebrowser=EcoGetVersion();
//alert(usebrowser);
	if ( usebrowser=='Safari' || usebrowser=='Opera' || usebrowser=='Firefox'){
    setTimeout( '_accessing('+accessing_id_name+')', 1 );
		// jQuery('#'+accessing_id_name).show();
	}
	else{
		setTimeout( '_accessing('+accessing_id_name+')', 1 );
	}
}

//
function _accessing(){
console.log( $('#'+accessing_id_name) );
	$('#'+accessing_id_name).show();
}

//
function hide_accessing(){
	if (jQuery('#'+accessing_id_name)){
		jQuery('#'+accessing_id_name).hide();
	}
}

//----------------------------------------------- EcoGetVersion
function EcoGetVersion(){
	//browser
	if (navigator.userAgent.indexOf('MSIE 3.',0) != -1){usebrowser='IE3';}
	else if (navigator.userAgent.indexOf('MSIE 4.',0) != -1){usebrowser='IE4';}
	else if (navigator.userAgent.indexOf('MSIE 5.',0) != -1){usebrowser='IE5';}
	else if (navigator.userAgent.indexOf('MSIE 6.',0) != -1){usebrowser='IE6';}
	else if (navigator.userAgent.indexOf('MSIE 7.',0) != -1){usebrowser='IE7';}
	else if (navigator.userAgent.indexOf('MSIE 8.',0) != -1){usebrowser='IE8';}
	else if (navigator.userAgent.indexOf('MSIE 9.',0) != -1){usebrowser='IE9';}
	else if (navigator.userAgent.indexOf('Netscape/7.',0) != -1){usebrowser='NN7';}
	else if (navigator.userAgent.indexOf('Netscape/6.',0) != -1){usebrowser='NN6';}
	else if (navigator.userAgent.indexOf('Mozilla/4.',0) != -1){usebrowser='NN4';}
	else if (navigator.userAgent.indexOf('Chrome',0) != -1){usebrowser='Chrome';}
	else if (navigator.userAgent.indexOf('Safari',0) != -1){usebrowser='Safari';}
	else if (navigator.userAgent.indexOf('Firefox',0) != -1){usebrowser='Firefox';}
	else if (navigator.userAgent.indexOf('Gecko',0) != -1){usebrowser='other_Gecko_NN';}
	else if (navigator.userAgent.indexOf('Opera',0) != -1){usebrowser='Opera';}
	else {usebrowser='other';}

	return (usebrowser);
}
