// notrepeat.js (c)2002-2015 econosys system
// http://www.econosys.jp/system/
//
// Version
//	1.0  とりあえず作成
//  1.1  アクセスアイコン対応

var nrpt_sent     = 0;
var nrpt_senttime = 0;
var nrpt_show_id  = '';

function notrepeat( nrpt_show_id ){
	var nowtime = 0;
	myD = new Date();
	nowtime = myD.getTime()/1000;
	nowtime = Math.floor(nowtime);
	if ( nrpt_sent === 0 || ( nowtime-nrpt_senttime) >= 3  ){
    $(window).unload(function(){
      nrpt_sent = 0;
      if ( $(nrpt_show_id).is(':visible') ){ $(nrpt_show_id).hide(); }
    });
		nrpt_sent=1; nrpt_senttime = nowtime;
    setTimeout( function() {
      $(nrpt_show_id).show();
    }, 1 );
		return true;
  }
	else { return false; }
}
