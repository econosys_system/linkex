<?php

require_once("../admin_manage/phplib/flatframe/textdb.php");
require_once("../admin_manage/phplib/flatframe/exdate.php");

$data_id = $_GET['data_id'];


// data_dt
$data_dt = new textdb( "../admin_manage/phplib/admin_manage/textdb.yml", 'data_dt', "../data/" );
$data_loop = $data_dt->select(array(
  'data_id'       => $data_id ,
),0,1);
$hash = $data_loop[0];
if (! isset($hash['data_id']) ){die("error:データ({$data_id})が取得できません");}


// count_dt
$count_dt = new textdb( "../admin_manage/phplib/admin_manage/textdb.yml", 'count_dt', "../data/" );

$t = new exdate();
list($year, $month, $day, $hour, $min, $sec) = $t->now();
$insert_hash = array(
  'data_id'     => $hash['data_id'] ,
  'data_name'   => $hash['data_name'] ,
  'access_date' => "{$year}/{$month}/{$day} {$hour}:{$min}:{$sec}",
);


$result_id = $count_dt->insert( $insert_hash );


header('Content-type: image/gif');
readfile('spacer.gif');

function dump($data){
  print "\n".'<pre style="text-align:left;">'."\n";
  print_r($data);
  print "</pre>\n";
}
